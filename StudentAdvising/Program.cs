﻿

namespace StudentAdvising;
class Program
{
    static void Main(string[] args)
    {
        string command = "";
        do
        {
            string name;
            string major_code = "";
            string major = "";
            string classification = "";
            string location = "";


            Console.Write("What is the student's name? ");

            try
            {
                name = Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                continue;
            }
            Console.Write("What is the student's major? ");

            try
            {
                major_code = Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
            Console.Write("What is the student's classification? ");

            try
            {
                classification = Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }

            GetData(in major_code, in classification, out major, out location);

            Console.WriteLine("Advising for {0} {1} majors: {2}", major, classification, location);


            Console.Write("Do you want to enter another ? (Y / N)? ");
            command = Console.ReadLine();
        } while (command != "N");
    }
    //STILL TO DO: What if bad classification
    static void GetData(
        in string major_code,
        in string classification,
        out string major,
        out string location)
    {

        major = "";
        location = "";
        switch (major_code)
        {

            //Major Name of Major
            //Code
            //BIOL
            //Biology
            //CSCI
            //Computer Science
            //Sheppard Hall, Room 314
            //ENG
            //English
            //HIST
            //History
            //MKT
            //Marketing
            //Department Office
            //Freshman - Sophomore: Science Bldg, Room 310 Junior - Senior: Science Bldg, Room 311
            //Freshmen: Kerr Hall, Room 201 Sophomore - Senior: Kerr Hall, Room 312 Kerr Hall, Room 114
            //Freshman - Junior: Westly Hall, Room 310 Senior: Westly Hall, Room 313

            case "BIOL":
                major = "Biology";
                //Freshman - Sophomore: Science Bldg, Room 310 Junior - Senior: Science Bldg, Room 311
                switch (classification)
                {
                    case string i when classification == "Freshman" || classification == "Sophomore":
                        location = "Science Bldg, Room 310";
                        break;
                    case string i when classification == "Junior" || classification == "Senior":
                        location = "Science Bldg, Room 311";
                        break;
                    default:
                        break;
                }
                break;
            case "CSCI":
                major = "Computer Science";
                switch (classification)
                {
                    default:
                        location = "Sheppard Hall, Room 314";
                        break;
                }
                break;
            case "ENG":
                major = "English";
                //Freshmen: Kerr Hall, Room 201 Sophomore - Senior: Kerr Hall, Room 312 Kerr Hall, Room 114
                switch (classification)
                {
                    case string i when classification == "Freshman":
                        location = "Kerr Hall, Room 201";
                        break;
                    case string i when classification == "Sophomore" || classification == "Junior" || classification == "Senior":
                        location = "Kerr Hall, Room 312";
                        break;
                    default:
                        break;
                }
                break;
            case "HIST":
                major = "History";
                switch (classification)
                {
                    default:
                        location = "Kerr Hall, Room 114";
                        break;
                }
                break;
            case "MKT":
                major = "Marketing";
                //Freshman - Junior: Westly Hall, Room 310 Senior: Westly Hall, Room 313
                switch (classification)
                {
                    case string i when classification == "Senior":
  
                        location = "Westly Hall, Room 313";
                        break;
                    default:
                        location = "Westly Hall, Room 310";
                        break;
                }
                break;
            default:
                break;
        }
    }
}





//    Major Name of Major
//Code
//BIOL Biology
//CSCI Computer Science
//ENG English
//HIST History
//MKT
//Marketing
//Department Office
//Freshman - Sophomore:
//Junior - Senior: Science Bldg, Room 311
//Sheppard Hall, Room 314
//Freshmen: Kerr Hall, Room 201
//Sophomore - Senior: Kerr Hall, Room 312
//Kerr Hall, Room 114
//Freshman - Junior: Westly Hall, Room 310
//Senior: Westly Hall, Room 313
//Science Bldg, Room 310






//Major Name of Major
//Code
//BIOL
//Biology
//CSCI
//Computer Science
//Sheppard Hall, Room 314
//ENG
//English
//HIST
//History
//MKT
//Marketing
//Department Office
//Freshman - Sophomore: Science Bldg, Room 310 Junior - Senior: Science Bldg, Room 311
//Freshmen: Kerr Hall, Room 201 Sophomore - Senior: Kerr Hall, Room 312 Kerr Hall, Room 114
//Freshman - Junior: Westly Hall, Room 310 Senior: Westly Hall, Room 313
