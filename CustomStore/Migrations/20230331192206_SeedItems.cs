﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CustomStore.Migrations
{
    /// <inheritdoc />
    public partial class SeedItems : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "EquipmentItems",
                columns: new[] { "Id", "Description", "Name", "Price", "Quantity", "Sport" },
                values: new object[] { 1, "Golf Club", "Ping Driver", 499.99000000000001, 10, "Golf" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "EquipmentItems",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
