﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CustomStore.Migrations
{
    /// <inheritdoc />
    public partial class SeedItems2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "EquipmentItems",
                columns: new[] { "Id", "Description", "Name", "Price", "Quantity", "Sport" },
                values: new object[] { 2, "Hockey Helmet", "Bauer Helmet 150", 150.99000000000001, 30, "Hockey" });

            migrationBuilder.InsertData(
                table: "FacilityItems",
                columns: new[] { "Id", "Description", "Name", "Price", "Quantity", "Sport", "Weight" },
                values: new object[] { 4, "Regulation Hockey Goal", "NHL Regulation Hockey Goal", 859.99000000000001, 3, "Hockey", 125.5 });

            migrationBuilder.InsertData(
                table: "FootwearItems",
                columns: new[] { "Id", "Description", "Gender", "Name", "Price", "Quantity", "Size", "Sport" },
                values: new object[] { 3, "Baseball Cleats", 1, "Under Armour Baseball Cleats", 95.989999999999995, 6, 10.0, "Baseball" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "EquipmentItems",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "FacilityItems",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "FootwearItems",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
