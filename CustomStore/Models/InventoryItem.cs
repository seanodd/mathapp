﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomStore
{
    public abstract class InventoryItem
    {


        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public double Price { get; set; }

        public int Quantity { get; set; }

        public InventoryItem(int id, string name, string description, double price, int quantity)
        {
            Id = id;
            Name = name;
            Description = description;
            Price = price;
            Quantity = quantity;
        }


        public virtual void Display()
        {
            Console.WriteLine($"{Id,4 } | {Name,35 } | {Description,28 } | {Price,6} | {Quantity,4} | ");
        }

    }
}
