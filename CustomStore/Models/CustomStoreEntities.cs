﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace CustomStore.Models
{
    public class CustomStoreEntities : DbContext
    {
        public DbSet<InventoryItem> InventoryItems { get; set; }
        //public DbSet<Movie> Movies { get; set; }
        //public DbSet<Category> Categories { get; set; }

        //public virtual DbSet<GetMoviesByCategory> GetMoviesByCategory { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=CustomStoreDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        }

        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        //        => optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Northwind;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

        protected override void OnModelCreating(
        ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<InventoryItem>().UseTpcMappingStrategy();
            modelBuilder.Entity<EquipmentItem>().ToTable("EquipmentItems");
            modelBuilder.Entity<FacilityItem>().ToTable("FacilityItems");
            modelBuilder.Entity<FootwearItem>().ToTable("FootwearItems");


            //modelBuilder.Entity<Category>()
            //.HasMany(category => category.Movies)
            //.WithOne(movie => movie.Category);  // USED in BLOCKBUSTER

            //.HasForeignKey(movie => .AuthorID); //NOT USED

            //modelBuilder.Entity<GetMoviesByCategory>(entity =>
            //{
            //    entity
            //    .HasNoKey();
            //});           //USED FOR STORED PROC

            modelBuilder.Entity<EquipmentItem>().HasData(
            new EquipmentItem(1, "Ping Driver", "Golf Club", 499.99, 10, "Golf"),
            new EquipmentItem(2, "Bauer Helmet 150", "Hockey Helmet", 150.99, 30, "Hockey")
            );

            modelBuilder.Entity<FootwearItem>().HasData(
            new FootwearItem(3, "Under Armour Baseball Cleats", "Baseball Cleats", 95.99, 6, "Baseball", Gender.Male, 10)
            );

            modelBuilder.Entity<FacilityItem>().HasData(
new FacilityItem(4, "NHL Regulation Hockey Goal", "Regulation Hockey Goal", 859.99, 3, "Hockey", 125.50)
);

         


            //{
            //    CategoryID = 1,
            //    CategoryName = "Romantic Comedy"
            //},

            //            modelBuilder.Entity<Category>().HasData(
            //            new Category
            //            {
            //                CategoryID = 1,
            //                CategoryName = "Romantic Comedy"
            //            },
            //            new Category
            //            {
            //                CategoryID = 2,
            //                CategoryName = "Action"
            //            },
            //            new Category
            //            {
            //                CategoryID = 3,
            //                CategoryName = "Kids"
            //            },
            //            new Category
            //            {
            //                CategoryID = 4,
            //                CategoryName = "Science Fiction"
            //            },
            //            new Category
            //            {
            //                CategoryID = 5,
            //                CategoryName = "Family"
            //            },
            //            new Category
            //            {
            //                CategoryID = 6,
            //                CategoryName = "Western"
            //            }
            //            );
            //            modelBuilder.Entity<Movie>().HasData(
            //                new {
            //                           Title = "2001: A Space Odyssey",
            //                           MovieID = 1,
            //                           Description = "When Dr. Dave Bowman (Keir Dullea) and other astronauts are sent on a mysterious mission, their ship's computer system, HAL, begins to display increasingly strange behavior.",
            //                           CategoryID = 4
            //                 },
            //                new
            //                {
            //                    Title = "Star Wars: Episode IV - A New Hope",
            //                    MovieID = 2,
            //                    Description = "Movie about Luke Skywalker",
            //                    CategoryID = 4
            //                },
            //                new
            //                {
            //                    Title = "Blade Runner",
            //                    MovieID = 3,
            //                    Description = "Movie about Blade Runners",
            //                    CategoryID = 4
            //                },
            //                new
            //                {
            //                    Title = "The Matrix",
            //                    MovieID = 4,
            //                    Description = "Movie about Neo",
            //                    CategoryID = 4
            //                },
            //                new
            //                {
            //                    Title = "The Dark Knight",
            //                    MovieID = 5,
            //                    Description = "Movie about batman",
            //                    CategoryID = 4
            //                },
            //                new
            //                {
            //                    Title = "Mad Max",
            //                    MovieID = 6,
            //                    Description = "Movie about Max.",
            //                    CategoryID = 2
            //                },
            //                new
            //                {
            //                    Title = "Aliens",
            //                    MovieID = 7,
            //                    Description = "Movie about Aliens",
            //                    CategoryID = 2
            //                },
            //                new
            //                {
            //                    Title = "Die Hard",
            //                    MovieID = 8,
            //                    Description = "Movie about NYC police officer saving hostages from building",
            //                    CategoryID = 2
            //                },
            //                new
            //                {
            //                    Title = "Terminator 2: Judgment Day",
            //                    MovieID = 9,
            //                    Description = "Movie about Terminators",
            //                    CategoryID = 2
            //                },
            //                new
            //                {
            //                    Title = "Casino Royale",
            //                    MovieID = 10,
            //                    Description = "Movie about James bond",
            //                    CategoryID = 2
            //                },
            /////////////////////////////////////////
            //                new
            //                {
            //                    Title = "Notting Hill",
            //                    MovieID = 11,
            //                    Description = "Movie with Hugh Grant",
            //                    CategoryID = 1
            //                },
            //                new
            //                {
            //                    Title = "When Harry Met Sally",
            //                    MovieID = 12,
            //                    Description = "Movie about Harry",
            //                    CategoryID = 1
            //                },
            //                new
            //                {
            //                    Title = "Beauty and the Beast",
            //                    MovieID = 13,
            //                    Description = "Movie about a Beast",
            //                    CategoryID = 3
            //                },
            //                new
            //                {
            //                    Title = "Toy Story",
            //                    MovieID = 14,
            //                    Description = "Story about toys",
            //                    CategoryID = 3
            //                },
            //                new
            //                {
            //                    Title = "The Goonies",
            //                    MovieID = 15,
            //                    Description = "Movie about Kids on an adventure",
            //                    CategoryID = 5
            //                },
            //                new
            //                {
            //                    Title = "E.T. The Extra-Terrestrial",
            //                    MovieID = 16,
            //                    Description = "Movie about ET",
            //                    CategoryID = 5
            //                },
            //                new
            //                {
            //                    Title = "High Noon",
            //                    MovieID = 17,
            //                    Description = "Movie about cowboys",
            //                    CategoryID = 6
            //                }
            //                 );
        }
    }
}





