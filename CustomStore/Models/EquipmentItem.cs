﻿namespace CustomStore
{
    public class EquipmentItem : InventoryItem
    {
        public EquipmentItem(int id, string name, string description, double price, int quantity, string sport) : base(id, name, description, price, quantity)
        {
            Sport = sport;
        }

        public string Sport { get; set; }


    }
}