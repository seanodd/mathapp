﻿namespace CustomStore
{
    public class FacilityItem : InventoryItem
    {
        public FacilityItem(int id, string name, string description, double price, int quantity, string sport, double weight) : base(id, name, description, price, quantity)
        {
            Sport = sport;
            Weight = weight;
        }

        public string Sport { get; set; }
        public double Weight { get; set; }


    }
}