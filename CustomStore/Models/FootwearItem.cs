﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomStore
{
    public class FootwearItem : InventoryItem
    {


        public string Sport { get; set; }
        public Gender Gender { get; set; }
        public double Size { get; set; }

        public FootwearItem(int id, string name, string description, double price, int quantity, string sport, Gender gender, double size) : base(id, name, description, price, quantity)
        {
            Sport = sport;
            Gender = gender;
            Size = size;
        }

        public override void Display()
        {
            Console.WriteLine($"{Id,4 } | {Name,35 } | {Description,28 } | {Price,6} | {Quantity,4} | {Sport,10 } | {Gender,3} | {Size,3} | ");
        }
    }
}
