﻿namespace CustomStore;

class Program
{
    static void Main(string[] args)
    {


        List<InventoryItem> items = new List<InventoryItem>();
        items.Add(new EquipmentItem(1, "Ping Driver", "Golf Club", 499.99, 10,"Golf"));
        items.Add(new EquipmentItem(2, "Bauer Helmet 150", "Hockey Helmet", 150.99, 30, "Hockey"));
        items.Add(new FootwearItem(3, "Under Armour Baseball Cleats", "Baseball Cleats", 95.99, 6,"Baseball",Gender.Male,10));
        items.Add(new FacilityItem(4, "NHL Regulation Hockey Goal", "Regulation Hockey Goal", 859.99, 3, "Hockey",125.50));

        //Apparel
        //    Footwear
        //    fitness
        //    facility
        //    outdoor
        //    equipnent

        Console.WriteLine("------------------- ORIGINAL LIST ----------------------");
        foreach (InventoryItem i in items)
        {
            i.Display();
        }


        string command;
        do
        {

            Console.WriteLine("What do you want to do? ");
            Console.WriteLine("1 - Find items that match type(derived class name) ");
            Console.WriteLine("2 - Find items that fall within a price range ");
            Console.WriteLine("3 - Find items that match a keyword in the description ");
            Console.WriteLine("4 - Show all items ");
            Console.WriteLine("5 - Add an item ");
            Console.WriteLine("6 - Quit ");
            Console.WriteLine("Enter your command: ");
            command = Console.ReadLine();
            string inventoryType = "";
            double deg_in_cel = 0.0;
            if (command == "1")
            {

                Console.WriteLine("Please enter a valid InventoryType ( EquipmentItem, FootwearItem, FacilityItem) : ");


                try
                {
                    inventoryType = Console.ReadLine();
                }
                catch (Exception ex)
                {

                    Console.WriteLine($"Please enter a valid InventoryType: {ex.Message}");
                    continue;
                }

                var itemQuery = from i in items
                                where i.GetType().Name == inventoryType
                                select i;
                foreach (InventoryItem item in itemQuery)
                {
                    //Console.WriteLine(item.GetType().Name + " | " + item.Name);
                    item.Display();
                }

                continue;
            }
            if (command == "2")
            {


            }
            if (command == "4")
            {
                foreach (InventoryItem i in items)
                {
                    i.Display();
                }

            }
            else if (command != "Quit")
            {
                Console.WriteLine("**Error: unrecognized command");
            }
        } while (command != "Quit");
    }
}