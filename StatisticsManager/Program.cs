﻿
namespace StatisticsManager;
class Program
{
    static void Main(string[] args)
    {

        double[] myScores = { 92, 98, 84, 76, 89, 97, 99 };
        double[] yourScores = { 82, 98, 94, 88, 92, 100 };
        //double[] scores = { 93, 86, 73, 79, 83, 100, 94 };

        Console.WriteLine("myScores Average  : " + GetAverage(myScores));
        Console.WriteLine("yourScores Average: " + GetAverage(yourScores));
        Console.WriteLine("---------------------------------------------");
        Console.WriteLine("myScores Median  : " + GetMedian(myScores));
        Console.WriteLine("yourScores Median: " + GetMedian(yourScores));
        Console.WriteLine("---------------------------------------------");
        Console.WriteLine("myScores High  : " + GetLargestValue(myScores));
        Console.WriteLine("yourScores High: " + GetLargestValue(yourScores));
        Console.WriteLine("---------------------------------------------");
        Console.WriteLine("myScores Low  : " + GetSmallestValue(myScores));
        Console.WriteLine("yourScores Low: " + GetSmallestValue(yourScores));

    }

    public static double GetAverage(double[] scores)
    {
        double totalScore = 0.0;
        double average = 0.0;
        for (int i = 0; i < scores.Length; i++)
        {
            totalScore = totalScore + scores[i];
        }
        average = totalScore / scores.Length;
        return average;
    }
    public static double GetMean(double[] scores)
    {
        return 0;
    }
    public static double GetMedian(double[] scores)
    {
        Array.Sort(scores);
        if (scores.Length % 2 != 0)
        {
            int middleIndex = scores.Length / 2;
            return scores[middleIndex];
        }
        else
        {
            int middleIndex = scores.Length / 2;
            return (scores[middleIndex] + scores[middleIndex - 1]) / 2;
        }
    }
    //public compareAscendingNumber(a, b)
    //{
    //    // if a is smaller, a-b is negative so don't swap!
    //    return a - b;
    //}

    public static double GetLargestValue(double[] scores)
    {
        Array.Sort(scores);
        return (scores[scores.Length - 1]);
    }
    public static double GetSmallestValue(double[] scores)
    {
        Array.Sort(scores);
        return (scores[0]);
    }
}



