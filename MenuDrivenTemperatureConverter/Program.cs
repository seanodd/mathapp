﻿class Program
{
    static void Main(string[] args)
    {
        string command;
        do
        {
            Console.WriteLine("CtoF  -  FtoC  -  Quit");
            Console.Write("What would you like to do? ");
            command = Console.ReadLine();
            double deg_in_far = 0.0;
            double deg_in_cel = 0.0;
            if (command == "FtoC")
            {

                Console.Write("Please enter the temperature in Farenheight: ");

               
                try
                {
                    deg_in_far = Convert.ToDouble(Console.ReadLine());
                }
                catch (Exception ex)
                {

                    Console.WriteLine($"Please enter a valid number: {ex.Message}");
                    continue;
                }
                deg_in_cel = ConvertFtoC(deg_in_far);
                Console.WriteLine("Celsius = {0:#,##0.0;(#,##0.0)}", deg_in_cel);
                continue;
            }
            if (command == "CtoF")
            {

                Console.Write("Please enter the temperature in Celsius: ");
                try
                {
                     deg_in_cel = Convert.ToDouble(Console.ReadLine());
                }
                catch (Exception ex)
                {

                    Console.WriteLine($"Please enter a valid number: {ex.Message}");
                    continue;
                }
                deg_in_far = ConvertCtoF(deg_in_cel);
                Console.WriteLine("Farenheight = {0:#,##0.0;(#,##0.0)}", deg_in_far);
                continue;
            }
            else if (command != "Quit")
            {
                Console.WriteLine("**Error: unrecognized command");
            }
        } while (command != "Quit");

    }
    static double ConvertFtoC(double deg_in_far)
    {
        return (double)(deg_in_far - 32) * (5.0 / 9.0);
    }
    static double ConvertCtoF(double deg_in_cel)
    {
        return (double)((deg_in_cel * 9.0) / 5.0) + 32.0;
    }

}