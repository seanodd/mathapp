﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManager
{
    public class Car : Asset
    {

        public Car(long assetId, string description, DateTime dateAcquired, double originalCost, string modelYear, double odometerReading) : base(assetId, description, dateAcquired, originalCost)
        {
            ModelYear = modelYear;
            OdometerReading = odometerReading;
        }

        public string ModelYear { get; set; }
        public double OdometerReading { get; set; }
        public override double GetValue()
        {
            //Console.WriteLine("--- Car - GetValue for " + this.ModelYear + ": Odometer" + this.OriginalCost.ToString());
            return this.OriginalCost;
        }
    }
}
