﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManager
{
    public class Asset
    {
        public Asset(long assetId, string description, DateTime dateAcquired, double originalCost)
        {
            AssetId = assetId;
            Description = description;
            DateAcquired = dateAcquired;
            OriginalCost = originalCost;
        }

        public Int64 AssetId { get; set; }
        public string Description { get; set; }
        public DateTime DateAcquired { get; set; }
        public double OriginalCost { get; set; }

        public virtual double GetValue()
        {
            Console.WriteLine("--- Asset - GetValue for " + this.Description + ": " + this.OriginalCost.ToString());
            return this.OriginalCost;
        }
    }
}
