﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManager
{
    // The derived class
    public class Stock : Asset
    {
        //public Stock(string stockTicker, double currentSharePrice, double numberOfShares)
        //{
        //    StockTicker = stockTicker;
        //    CurrentSharePrice = currentSharePrice;
        //    NumberOfShares = numberOfShares;
        //}

        public Stock(long assetId,
                     string description,
                     DateTime dateAcquired,
                     double originalCost,
                     string stockTicker, double currentSharePrice, double numberOfShares) :
            base(assetId,
                 description,
                 dateAcquired,
                 originalCost)
        {
            StockTicker = stockTicker;
            CurrentSharePrice = currentSharePrice;
            NumberOfShares = numberOfShares;
        }

        public string StockTicker { get; set; }
        public double CurrentSharePrice { get; set; }

        public double NumberOfShares { get; set; }

        public override double GetValue()
        {
            //Console.WriteLine("--- Stock - GetValue for " + this.Description + ": " + this.OriginalCost.ToString());
            return this.CurrentSharePrice * this.NumberOfShares;
        }
    }
}
