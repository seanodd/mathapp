﻿using AssetManager;

namespace DataScrubbing;

class Program
{
    static void Main(string[] args)
    {
        // NEED TO UPDATE CAR VALUE Depreciation and LOOP FOR Stock
        Asset[] assets = new Asset[6];
        assets[0] = new Stock(0987098, "Tesla Stock", new DateTime(2020, 10, 10), 1500.00, "TSLA", 194.00, 10);
        assets[1] = new Stock(5649789, "Apple Shares", new DateTime(2021, 06, 10), 1200.00, "AAPL", 157.90, 11);
        assets[2] = new Stock(2316549, "Ford Shares", new DateTime(2021, 03, 10), 164.00, "F", 11.74, 14);

        assets[3] = new Car(1000001, "Jeep Grand Cherokee", new DateTime(2022, 10, 10), 47000.00, "2022", 5000.00);
        assets[4] = new Car(1000002, "Ford Explorer", new DateTime(2020, 02, 10), 38000.00, "2020", 20000.00);
        assets[5] = new Car(1000003, "Porsche Carrera ", new DateTime(2023, 03, 10), 106000, "2023", 500);


        foreach (Asset a in assets)
        {
            Console.WriteLine($"{a.Description, 25 } | {a.DateAcquired, 25 } | {a.OriginalCost, 8} | {a.GetValue()}");
        }
        Console.WriteLine("------------------  CARS  -----------------");
        foreach (Asset a in assets)
        {
            Car c = null;
            if (a is Car)
            {
                c = (Car)a;
            }

            if (c!= null )
            {
                Console.WriteLine($"{c.Description,25 } | {c.DateAcquired,25 } | {c.ModelYear,8} | {c.OdometerReading}");
            }
            
        }

    }
}