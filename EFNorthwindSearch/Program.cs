﻿using EFNorthwindSearch.Models;
using Microsoft.EntityFrameworkCore;

namespace EFNorthwindSearch;

class Program
{
    static void Main(string[] args)
    {
        using (var context = new NorthwindContext())
        {
            //Console.WriteLine("Database Connected");
            //Console.WriteLine();
            //Console.WriteLine("Listing Category Sales For 1997s");
            //db.CategorySalesFor1997s.ToList().ForEach(x => Console.WriteLine(x.CategoryName));
            //Console.WriteLine();
            //Console.WriteLine("Listing Products Above Average Prices");
            //db.ProductsAboveAveragePrices.ToList().ForEach(x => Console.WriteLine(x.ProductName));
            //Console.WriteLine();
            //Console.WriteLine("Listing Territories");
            //db.Territories.ToList().ForEach(x => Console.WriteLine(x.TerritoryDescription));

            ////var context = new NorthwindContext();
            //var allShippersQuery = from s in db.Shippers
            //                       select s;
            //foreach (Shipper s in allShippersQuery)
            //{
            //    Console.WriteLine(
            //    $"#{s.ShipperId} {s.CompanyName} Ph: {s.Phone}");
            //}

            //var context = new NorthwindContext();


            Console.WriteLine("-------------DisplayCategoryCount------------------");
            DisplayCategoryCount();
            Console.WriteLine("----------------DisplayTotalInventoryValue---------------");
            DisplayTotalInventoryValue();
            Console.WriteLine("---------------AddAShipper----------------");
            AddAShipper();
            Console.WriteLine("----------------DisplayShippers---------------");
            DisplayShippers();
            Console.WriteLine("----------------ChangeShipperName---------------");
            ChangeShipperName();
            Console.WriteLine("-----------------DisplayShippers--------------");
            DisplayShippers();
            Console.WriteLine("-----------------DeleteShipper--------------");
            DeleteShipper();
            Console.WriteLine("----------------DisplayShippers---------------");
            DisplayShippers();

            //DisplayAllProducts();
            //Console.WriteLine("-----------------DisplayProductsInCategory--------------");


            //DisplayProductsInCategory("Meat/Poultry");
            
            Console.WriteLine("---------------DisplayProductsForSupplier----------------");
            DisplayProductsForSupplier("Specialty Biscuits, Ltd.");
            ////cn.Close();
            //Console.WriteLine("-------------------------------");
            //DisplaySupplierProductCounts();
            Console.WriteLine("---------------DisplaySupplierProductCounts2----------------");
            DisplaySupplierProductCounts2();

            //Console.WriteLine("---------------DisplayCustomersandSuppliersByCity----------------");
            //DisplayCustomersandSuppliersByCity();
            Console.WriteLine("--------------DisplayTenMostExpensiveProducts-----------------");
            DisplayTenMostExpensiveProducts();
            Console.WriteLine("-------------DisplaySalesByCategory------------------");
            DisplaySalesByCategory("Beverages", "1997");
            Console.WriteLine("-------------------------------");


            Console.WriteLine("Product categories are:");
            var allCategoriesQuery = from c in context.Categories
                                     select c;
            foreach (Category c in allCategoriesQuery)
            {
                Console.WriteLine(
                $"{c.CategoryName}");
            }

            Console.Write("What type of product would you like to see? ");
            string category = Console.ReadLine();

            Console.WriteLine("ID Product Price");
            Console.WriteLine("-------- - -----");
            var prodAndCategoryQuery =
                from c in context.Categories
                join p in context.Products
                on c.CategoryId equals p.CategoryId
                where c.CategoryName == category
                orderby c.CategoryName
                select new
                {
                    ProductId = p.ProductId,

                    Category = c.CategoryName,
                    Name = p.ProductName,
                    Price = p.UnitPrice
                };
            foreach (var g in prodAndCategoryQuery)
            {
                Console.WriteLine($"{g.ProductId,5} {g.Name,20} {g.Price,8}");
            }

        }
    }

    private static void DisplaySalesByCategory(string v1, string v2)
    {
        Console.WriteLine("------!!!!!INCOMPLETE!!!!!-------------------------");
    }

    private static void DisplayTenMostExpensiveProducts()
    {
        using (var context = new NorthwindContext())
        {
            var mostExpensive =
                 context.TenMostExpensiveProductsResults.FromSqlInterpolated(
                 $"[Ten Most Expensive Products]").ToList();
            foreach (var p in mostExpensive)
            {
                Console.WriteLine(
                $"{p.TenMostExpensiveProducts} -- {p.UnitPrice:C}");
            }
        }
    }

    private static void DisplaySupplierProductCounts2()
    {
        //Select Suppliers.CompanyName, count(*) as suppc " +
        //        "From Products JOIN Categories on Products.CategoryID =Categories.CategoryID " +
        //        "JOIN Suppliers on Suppliers.SupplierID = Products.SupplierID " +
        //        "GROUP BY Suppliers.CompanyName

        //using (var context = new NorthwindContext())
        //{
        //    var prodAndCategoryQuery =
        //         from c in context.Categories
        //         join p in context.Products
        //          on c.CategoryId equals p.CategoryId
        //         join s in context.Suppliers
        //          on p.SupplierId equals s.SupplierId
        //          into suppProdGroup
                 
        //         select new
        //         {
        //             productID = p.ProductId,
        //             productName = p.ProductName,
        //             unitsInStock = p.UnitsInStock,
        //             unitPrice = p.UnitPrice,
        //             categoryName = c.CategoryName,
        //             companyName = s.CompanyName

        //         };
        //    foreach (var g in prodAndCategoryQuery)
        //    {
        //        //Console.WriteLine($"{g.Category,-20} {g.ProductName}");
        //        Console.WriteLine(
        //                       $"[{g.productID,4}] {g.productName,40} - {g.unitsInStock,5} | {g.unitPrice,10:C} | {g.categoryName,15} | {g.companyName,10}");
        //    }

        //}

        //Console.WriteLine(
        //                 $"[{CompanyName,40}] {count,4}");


        Console.WriteLine("------!!!!!INCOMPLETE!!!!!-------------------------");
    }

    private static void DisplayProductsForSupplier(string inputSupplierName)
    {

        //"Select ProductID, ProductName, UnitsInStock,UnitPrice,CategoryName,CompanyName " +
        //        "From Products JOIN Categories on Products.CategoryID =Categories.CategoryID " +
        //        "JOIN Suppliers on Suppliers.SupplierID = Products.SupplierID " +
        //        " where CompanyName = '" + inputSupplierName + "'";
        using (var context = new NorthwindContext())
        {
            var prodAndCategoryQuery =
                 from c in context.Categories
                 join p in context.Products
                  on c.CategoryId equals p.CategoryId
                 join s in context.Suppliers
                  on p.SupplierId equals s.SupplierId
                 where s.CompanyName == inputSupplierName
                 orderby c.CategoryName
                 select new
                 {
                     productID = p.ProductId,
                     productName = p.ProductName,
                     unitsInStock = p.UnitsInStock,
                     unitPrice = p.UnitPrice,
                     categoryName = c.CategoryName,
                     companyName = s.CompanyName
                   
                 };
            foreach (var g in prodAndCategoryQuery)
            {
                //Console.WriteLine($"{g.Category,-20} {g.ProductName}");
                Console.WriteLine(
                               $"[{g.productID,4}] {g.productName,40} - {g.unitsInStock,5} | {g.unitPrice,10:C} | {g.categoryName,15} | {g.companyName,10}");
            }
        
    }

    }

    public static void DisplayCategoryCount()
    {
        using (var context = new NorthwindContext())
        {
            var categoriesCount =
                (from c in context.Categories
                 select c).Count();
            Console.WriteLine($"We have {categoriesCount} Categories");

        }
    }
    public static void DisplayTotalInventoryValue()
    {

        using (var context = new NorthwindContext())
        {
            //var query = from p in context.Products
            //            select new
            //            {
            //                TotalInventoryValue = (p.UnitsInStock * p.UnitPrice)
            //            };
            var TotalInventoryValue = context.Products.Sum(i => i.UnitPrice * i.UnitsInStock);
            Console.WriteLine($"TotalInventoryValue: {TotalInventoryValue} ");

        }
    }
    public static void AddAShipper()
    {

        using (var context = new NorthwindContext())
        {
            //var query = from p in context.Products
            //            select new
            //            {
            //                TotalInventoryValue = (p.UnitsInStock * p.UnitPrice)
            //            };
            Shipper shipper = new Shipper();
            shipper.Phone = "3134441122";
            shipper.CompanyName = "New Shipper";
            context.Shippers.Add(shipper);
            context.SaveChanges();


        }
        //SqlConnection cn = new SqlConnection(getConnectionString());
        //cn.Open();
        //string sql = "INSERT INTO Shippers VALUES ('New Shipper',3134441122)";
        //SqlCommand cmd = new SqlCommand(sql, cn);
        //int rowsChanged = cmd.ExecuteNonQuery();
        //Console.WriteLine($"{rowsChanged} rows changed");
    }
    public static void DisplayShippers()
    {
        using (var context = new NorthwindContext())
        {
            var allShippersQuery = from s in context.Shippers
                                   select s;
            foreach (Shipper s in allShippersQuery)
            {
                Console.WriteLine(
                $"#{s.ShipperId} {s.CompanyName} Ph: {s.Phone}");
            }

        }
    }

    public static void ChangeShipperName()
    {

        using (var context = new NorthwindContext())
        {     
            var shippers = (from s in context.Shippers where s.CompanyName == "New Shipper" select s).ToList();
            foreach (var s in shippers)
            {
                s.CompanyName = "Updated New Shipper";
            }
            context.SaveChanges();
        }
    }

    public static void DeleteShipper()
    {
        using (var context = new NorthwindContext())
        {
            var shippers = (from s in context.Shippers where s.CompanyName == "Updated New Shipper" select s).ToList();
            foreach (var s in shippers)
            {
                context.Shippers.Remove(s);
            }
            context.SaveChanges();
        }
    }

}