﻿namespace LINQGames;

class Program
{
    static void Main(string[] args)
    {

        List<Supplier> suppliers = new List<Supplier> {
new Supplier(101, "ACME", "acme.com"),
new Supplier(201, "Spring Valley", "spring-valley.com"),
};
        List<Product> products = new List<Product> {
new Product(1, "Dark Chocolate Bar", 4.99M, 10, 101),
new Product(2, "8 oz Guacamole", 5.99M, 27, 201),
new Product(3, "Milk Chocolate Bar", 3.99M, 16, 101),
new Product(4, "8 pkg Chicken Tacos", 15.99M, 7, 201)
};

        var query = from s in suppliers
                    select s.Name;
        foreach (string name in query)
        {
            Console.WriteLine(name);
        }

        query = from p in products
                where p.Price <= 5.00M
                select p.ProductName;
        foreach (string name in query)
        {
            Console.WriteLine(name);
        }

        var custQuery = from p in products
                where p.QuantityOnHand >= 10
                orderby p.QuantityOnHand descending
                select p;
        foreach (Product p in custQuery)
        {
            Console.WriteLine(p.ProductName + " | " + p.QuantityOnHand);
        }
    }
}