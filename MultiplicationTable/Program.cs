﻿namespace MultiplicationTable;
class Program
{
    static void Main(string[] args)
    {
        int[,] mathTable = CreateArray();
        for (int i = 0; i < mathTable.GetLength(0); i++) // 10
        {
            for (int j = 0; j < mathTable.GetLength(1); j++) // 5
            {
                mathTable[i, j] = i * j; // multiplication table
            }
        }
        
        // TOP ROW of NUMBERs 
        Console.Write("{0,4}", "");
        for (int k = 0; k < mathTable.GetLength(1); k++)
        {
            Console.Write("{0,6}", k);
        }
        Console.Write(Environment.NewLine);
        Console.Write("{0,4}", "");

        // TOP ROW of dashes 
        for (int m = 0; m < mathTable.GetLength(1); m++)
        {
                Console.Write("{0,6}","---");
        }
        Console.Write(Environment.NewLine);
        for (int i = 0; i < mathTable.GetLength(0); i++)
        {
            Console.Write($"{i} | ");
            for (int j = 0; j < mathTable.GetLength(1); j++)
            {

                Console.Write("{0,6}", mathTable[i, j]);
            }
            Console.Write(Environment.NewLine + Environment.NewLine);
        }

    }

    static public int[,] CreateArray()
    {
        int[,] table = new int[10, 5];
        return table;
    }
}