﻿namespace DataScrubbing;

class Program
{
    static void Main(string[] args)
    {
        string command;
        do
        {
            Console.WriteLine("CtoF  -  FtoC  -  Scrub  -  Scrub2  -  Scrub3  -  Quit");
            Console.Write("What would you like to do? ");
            command = Console.ReadLine();
            double deg_in_far = 0.0;
            double deg_in_cel = 0.0;
            string phoneNumber;
            char charToRemove;
            char charToReplaceWith;

            if (command == "FtoC")
            {

                Console.Write("Please enter the temperature in Farenheight: ");


                try
                {
                    deg_in_far = Convert.ToDouble(Console.ReadLine());
                }
                catch (Exception ex)
                {

                    Console.WriteLine($"Please enter a valid number: {ex.Message}");
                    continue;
                }
                deg_in_cel = ConvertFtoC(deg_in_far);
                Console.WriteLine("Celsius = {0:#,##0.0;(#,##0.0)}", deg_in_cel);
                continue;
            }
            if (command == "CtoF")
            {

                Console.Write("Please enter the temperature in Celsius: ");
                try
                {
                    deg_in_cel = Convert.ToDouble(Console.ReadLine());
                }
                catch (Exception ex)
                {

                    Console.WriteLine($"Please enter a valid number: {ex.Message}");
                    continue;
                }
                deg_in_far = ConvertCtoF(deg_in_cel);
                Console.WriteLine("Farenheight = {0:#,##0.0;(#,##0.0)}", deg_in_far);
                continue;
            }
            if (command == "Scrub")
            {

                Console.Write("Please enter a phone number: ");
                try
                {
                    phoneNumber = (Console.ReadLine());
                }
                catch (Exception ex)
                {

                    Console.WriteLine($"Please enter a valid phone number: {ex.Message}");
                    continue;
                }
                Console.WriteLine($"phoneNumber BEFORE = |{phoneNumber}|", phoneNumber);
                ScrubPhone(ref phoneNumber);
                Console.WriteLine($"phoneNumber AFTER  = |{phoneNumber}|", phoneNumber);
                continue;
            }
            if (command == "Scrub2")
            {

                Console.Write("Please enter a phone number: ");
                try
                {
                    phoneNumber = (Console.ReadLine());
                }
                catch (Exception ex)
                {

                    Console.WriteLine($"Please enter a valid phone number: {ex.Message}");
                    continue;
                }
                Console.Write("Please enter a character to remove from phone number: ");
                try
                {
                    charToRemove = Console.ReadKey().KeyChar; 
                }
                catch (Exception ex)
                {

                    Console.WriteLine($"Please enter a valid character to remove: {ex.Message}");
                    continue;
                }
                Console.WriteLine($"phoneNumber BEFORE = |{phoneNumber}|", phoneNumber);
                ScrubPhone(ref phoneNumber, charToRemove);
                Console.WriteLine($"phoneNumber AFTER  = |{phoneNumber}|", phoneNumber);
                continue;
            }
            if (command == "Scrub3")
            {

                Console.Write("Please enter a phone number: ");
                try
                {
                    phoneNumber = (Console.ReadLine());
                }
                catch (Exception ex)
                {

                    Console.WriteLine($"Please enter a valid phone number: {ex.Message}");
                    continue;
                }

                Console.Write("Please enter a character to remove from phone number: ");
                try
                {
                    charToRemove = Console.ReadKey().KeyChar;
                }
                catch (Exception ex)
                {

                    Console.WriteLine($"Please enter a valid character to remove: {ex.Message}");
                    continue;
                }

                Console.Write("Please enter a character to replace the previous character with: ");
                try
                {
                    charToReplaceWith = Console.ReadKey().KeyChar;
                }
                catch (Exception ex)
                {

                    Console.WriteLine($"Please enter a valid character to remove: {ex.Message}");
                    continue;
                }
                Console.WriteLine($"phoneNumber BEFORE = |{phoneNumber}|", phoneNumber);
                ScrubPhone(ref phoneNumber, charToRemove, charToReplaceWith);
                Console.WriteLine($"phoneNumber AFTER  = |{phoneNumber}|", phoneNumber);
                continue;
            }
            else if (command != "Quit")
            {
                Console.WriteLine("**Error: unrecognized command");
            }
        } while (command != "Quit");

    }
    static double ConvertFtoC(double deg_in_far)
    {
        return (double)(deg_in_far - 32) * (5.0 / 9.0);
    }
    static double ConvertCtoF(double deg_in_cel)
    {
        return (double)((deg_in_cel * 9.0) / 5.0) + 32.0;
    }
    public static void ScrubPhone(ref string phoneNumber)
    {
        phoneNumber = phoneNumber.Trim();
    }

    public static void ScrubPhone(ref string phoneNumber,
    char charToRemove)
    {
        Console.WriteLine($"ScrubPhone2 charToRemove = |{charToRemove}|", charToRemove);
        phoneNumber = phoneNumber.Replace(charToRemove.ToString(),String.Empty);
    }
    public static void ScrubPhone(ref string phoneNumber,
    char charToRemove, char charToReplaceWith)
    {
        phoneNumber = phoneNumber.Replace(charToRemove, charToReplaceWith);
    }
}

