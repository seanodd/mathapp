﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleManager
{
    public class Person : IComparable<Person>
    {
        public Person(string name, int age, string address, string city, string state, string zipCode)
        {
            Name = name;
            Age = age;
            Address = address;
            City = city;
            State = state;
            ZipCode = zipCode;
        }

        public string Name { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        public void HaveABirthday()
        {
            Age++;
        }
        public virtual void Display()
        {
            Console.WriteLine($"{Name,20 } | {Age,4 } | {Address,20} | {City, 12} | {State,3} | {ZipCode,8}  ");
        }

        public void Move()
        {

        }

        public int CompareTo(Person? other)
        {
            return Name.CompareTo(other.Name);
        }



    }
}
