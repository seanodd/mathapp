﻿namespace PeopleManager;

class Program
{
    static void Main(string[] args)
    {


        List<Person> people = new List<Person>();
        people.Add(new Person("Ian", 17, "93 NORTH 9TH STREET", "BROOKLYN", "NY", "11211"));
        people.Add(new Person("Elisha", 15, "380 WESTMINSTER ST", "PROVIDENCE", "RI", "02903"));
        people.Add(new Person("Ezra", 18, "177 MAIN STREET", "LITTLETON","NH", "03561"));
        people.Add(new Person("Siddalee", 16,"202 HARLOW ST", "BANGOR","ME", "04401"));
        people.Add(new Worker("Pursalane", 13, "46 FRONT STREET", "WATERVILLE", "ME", "04901", "Software Developer", 20000));
        people.Add(new Worker("Zephaniah", 11, "22 SUSSEX ST", "HACKENSACK", "NJ", "07601", "Accountant", 15000));
        Console.WriteLine("------------------- ORIGINAL LIST ----------------------");
        foreach (Person p in people)
        {
            p.Display();
        }
        people.Sort();
        Console.WriteLine("------------------- AFTER SORT BY NAME ----------------------");
        foreach (Person p in people)
        {
            p.Display();

        }
        people.Sort(new AscendingAgeSorter());
        Console.WriteLine("------------------- AFTER SORT BY AGE ASCENDING ----------------------");
        foreach (Person p in people)
        {
            p.Display();

        }

        people[4].HaveABirthday();
        people[4].HaveABirthday();
        people.Sort(new AscendingAgeSorter());
        Console.WriteLine("------------------- AFTER SORT BY AGE ASCENDING AFTER Ian Birthday x2 ----------------------");
        foreach (Person p in people)
        {
            p.Display();

        }

        people.Sort(new DescendingZipCodeSorter());
        Console.WriteLine("------------------- AFTER SORT BY ZIP Descending ----------------------");
        foreach (Person p in people)
        {
            p.Display();

        }

        people.Sort(new AscendingStateCitySorter());
        Console.WriteLine("------------------- AFTER SORT BY State then City ASCENDING ----------------------");
        foreach (Person p in people)
        {
            p.Display();

        }
    }
}