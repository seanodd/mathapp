﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleManager
{
    public class DescendingAgeSorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return y.Age - x.Age;
        }
    }
    public class AscendingAgeSorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return x.Age - y.Age;
        }
    }

    public class DescendingZipCodeSorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return y.ZipCode.CompareTo(x.ZipCode);
        }
    }
    public class AscendingZipCodeSorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return x.ZipCode.CompareTo(y.ZipCode);
        }
    }

    public class AscendingStateCitySorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            if (x.State.CompareTo(y.State) < 0)
                return -1;
            else if (x.State.CompareTo(y.State) == 0)
                return x.City.CompareTo(y.City);
            else
                return 1;
        }

    }
}