﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleManager
{
    public class Worker : Person
    {
        public Worker(string name, int age, string address, string city, string state, string zipCode, string jobTitle, int salary) : base(name, age, address, city, state, zipCode)
        {
            JobTitle = jobTitle;
            Salary = salary;
        }

        public string JobTitle { get; set; }
        public int Salary { get; set; }

        public override void Display()
        {
            Console.WriteLine($"{Name,20 } | {Age,4 } | {Address,20} | {City,12} | {State,3} | {ZipCode,8} | {JobTitle,20} | {Salary,10}  ");
        }
    }
}
