﻿
string command = "";
do
{
    Console.WriteLine("Commands -->");
    Console.WriteLine(" GET-QUOTE : Place an order");

    Console.WriteLine(" QUIT : Quit the program");
    Console.Write("Enter your command: ");
    command = Console.ReadLine();
    if (command == "GET-QUOTE")
    {
        string product_code = "";
        int quantity;
        double price = 0.0;
        double discount = 0.0;
        double total = 0.0;
        double final_total = 0.0;

        Console.Write("What is the product code: ");
        product_code = Console.ReadLine();

        Console.Write("What is the quantity: ");
        quantity = Convert.ToInt32(Console.ReadLine());

        string dayName = "";
        switch (product_code)
        {

            //BG - 127 $18.99 $17.00 $14.49
            //WRTR - 28 $125.00 $113.75 $99.99
            //GUAC - 8 $8.99 $8.99 $7.4
            case "BG-127":
                switch (quantity)
                {
                    case int i when quantity < 25:
                        price = 18.99;
                        break;
                    case int i when quantity > 24 && quantity < 51:
                        price = 17.00;
                        break;
                    case int i when quantity > 50:
                        price = 14.49;
                        break;
                    default:
                        break;
                }
                break;
            case "WRTR-28":
                switch (quantity)
                {
                    case int i when quantity < 25:
                        price = 125.00;
                        break;
                    case int i when quantity > 24 && quantity < 51:
                        price = 113.75;
                        break;
                    case int i when quantity > 50:
                        price = 99.99;
                        break;
                    default:
                        break;
                }
                break;
            case "GUAC-8":
                switch (quantity)
                {
                    case int i when quantity < 25:
                        price = 8.99;
                        break;
                    case int i when quantity > 24 && quantity < 51:
                        price = 8.99;
                        break;
                    case int i when quantity > 50:
                        price = 7.49;
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }

        total = quantity * price;
        if (quantity > 249)
        {
            discount = total * .15;
        }
        final_total = total - discount;

        Console.WriteLine($"product code         : {product_code}");
        Console.WriteLine($"quantity             : {quantity}");
        Console.WriteLine($"per-unit price       : {price:C}");
        Console.WriteLine($"price before discount: {total:C}");
        Console.WriteLine($"discount             : {discount:C}");
        Console.WriteLine($"price after discount : {final_total:C}");
    }

    else if (command != "QUIT")
    {
        Console.WriteLine("**Error: unrecognized command");
    }
} while (command != "QUIT");


//string product_code = "";
//int quantity;
//double price = 0.0;
//double discount = 0.0;
//double total = 0.0;
//double final_total = 0.0;

//Console.Write("What is the product code: ");
//product_code = Console.ReadLine();

//Console.Write("What is the quantity: ");
//quantity = Convert.ToInt32(Console.ReadLine());

//string dayName = "";
//switch (product_code)
//{

//    //BG - 127 $18.99 $17.00 $14.49
//    //WRTR - 28 $125.00 $113.75 $99.99
//    //GUAC - 8 $8.99 $8.99 $7.4
//    case "BG-127":
//        switch (quantity)
//        {
//            case int i when quantity < 25:
//                price = 18.99;
//                break;
//            case int i when quantity > 24 && quantity < 51:
//                price = 17.00;
//                break;
//            case int i when quantity > 50:
//                price = 14.49;
//                break;
//            default:
//                break;
//        }
//        break;
//    case "WRTR-28":
//        switch (quantity)
//        {
//            case int i when quantity < 25:
//                price = 125.00;
//                break;
//            case int i when quantity > 24 && quantity < 51:
//                price = 113.75;
//                break;
//            case int i when quantity > 50:
//                price = 99.99;
//                break;
//            default:
//                break;
//        }
//        break;
//    case "GUAC-8":
//        switch (quantity)
//        {
//            case int i when quantity < 25:
//                price = 8.99;
//                break;
//            case int i when quantity > 24 && quantity < 51:
//                price = 8.99;
//                break;
//            case int i when quantity > 50:
//                price = 7.49;
//                break;
//            default:
//                break;
//        }
//        break;
//    default:
//        break;
//}

//total = quantity * price;
//if (quantity > 249)
//{
//    discount = total * .15;
//}
//final_total = total - discount;

//Console.WriteLine($"product code         : {product_code}");
//Console.WriteLine($"quantity             : {quantity}");
//Console.WriteLine($"per-unit price       : {price:C}");
//Console.WriteLine($"price before discount: {total:C}");
//Console.WriteLine($"discount             : {discount:C}");
//Console.WriteLine($"price after discount : {final_total:C}");