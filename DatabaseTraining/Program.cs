﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Data;
// Set up a configuration object that can read your
// appsettings.json file

namespace DatabaseTraining;

class Program
{
    static void Main(string[] args)
    {


        //string? connStr = getConnectionString();
        //SqlConnection cn = new SqlConnection(connStr);
        //cn.Open();
        // do something here

        //string sql = "Select Count(*) From Products";
        //SqlCommand cmd = new SqlCommand(sql, cn);
        //int productCount = (int)cmd.ExecuteScalar();
        //Console.WriteLine($"We carry {productCount} products");



        ////
        ///
        Console.WriteLine("-------------------------------");
        DisplayCategoryCount();
        Console.WriteLine("-------------------------------");
        DisplayTotalInventoryValue();
        Console.WriteLine("-------------------------------");
        AddAShipper();
        Console.WriteLine("-------------------------------");
        DisplayShippers();
        Console.WriteLine("-------------------------------");
        ChangeShipperName();
        Console.WriteLine("-------------------------------");
        DisplayShippers();
        Console.WriteLine("-------------------------------");
        DeleteShipper();
        Console.WriteLine("-------------------------------");
        DisplayShippers();
        Console.WriteLine("-------------------------------");

        DisplayAllProducts();
        Console.WriteLine("-----------------DisplayProductsInCategory--------------");


        DisplayProductsInCategory("Meat/Poultry");
        Console.WriteLine("---------------DisplayProductsForSupplier----------------");

        DisplayProductsForSupplier("Specialty Biscuits, Ltd.");
        //cn.Close();
        //Console.WriteLine("-------------------------------");
        //DisplaySupplierProductCounts();
        Console.WriteLine("---------------DisplaySupplierProductCounts2----------------");
        DisplaySupplierProductCounts2();

        Console.WriteLine("---------------DisplayCustomersandSuppliersByCity----------------");
        DisplayCustomersandSuppliersByCity();
        Console.WriteLine("--------------DisplayTenMostExpensiveProducts-----------------");
        DisplayTenMostExpensiveProducts();
        Console.WriteLine("-------------DisplaySalesByCategory------------------");
        DisplaySalesByCategory("Beverages", "1997");
        Console.WriteLine("-------------------------------");

    }

    public static void DisplayAllProducts()
    {
        try
        {
            using (SqlConnection cn = new SqlConnection(getConnectionString()))
            {
                cn.Open();
                string sql =
                "Select ProductID, ProductName, UnitsInStock,UnitPrice " +
                "From Products ";
                using (SqlCommand cmd = new SqlCommand(sql, cn))
                {
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            int productID = (int)(dr["ProductID"]);
                            string productName = (string)dr["ProductName"];
                            int unitsInStock = (Int16)(dr["UnitsInStock"]);
                            decimal unitPrice = (decimal)(dr["UnitPrice"]);
                            Console.WriteLine(
                            $"[{productID,4}] {productName,40} - {unitsInStock,5} | {unitPrice,10:C}");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
    public static void DisplayProductsInCategory(string CategoryName)
    {
        try
        {
            using (SqlConnection cn = new SqlConnection(getConnectionString()))
            {
                cn.Open();
                string sql =
                "Select ProductID, ProductName, UnitsInStock,UnitPrice,CategoryName " +
                "From Products JOIN Categories on Products.CategoryID =Categories.CategoryID " +
                " where CategoryName = @catName";
                using (SqlCommand cmd = new SqlCommand(sql, cn))
                {
                    SqlParameter param = new SqlParameter("@catName", SqlDbType.NVarChar);
                    param.Value = CategoryName;
                    cmd.Parameters.Add(param);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            int productID = (int)(dr["ProductID"]);
                            string productName = (string)dr["ProductName"];
                            int unitsInStock = (Int16)(dr["UnitsInStock"]);
                            decimal unitPrice = (decimal)(dr["UnitPrice"]);
                            string categoryName = (string)(dr["CategoryName"]);
                            Console.WriteLine(
                            $"[{productID,4}] {productName,40} - {unitsInStock,5} | {unitPrice,10:C} | {categoryName,10}");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }

    }
    public static void DisplayProductsForSupplier(string inputSupplierName)
    {
        try
        {
            using (SqlConnection cn = new SqlConnection(getConnectionString()))
            {
                cn.Open();
                string sql =
                "Select ProductID, ProductName, UnitsInStock,UnitPrice,CategoryName,CompanyName " +
                "From Products JOIN Categories on Products.CategoryID =Categories.CategoryID " +
                "JOIN Suppliers on Suppliers.SupplierID = Products.SupplierID " +
                " where CompanyName = '" + inputSupplierName + "'";
                using (SqlCommand cmd = new SqlCommand(sql, cn))
                {
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            int productID = (int)(dr["ProductID"]);
                            string productName = (string)dr["ProductName"];
                            int unitsInStock = (Int16)(dr["UnitsInStock"]);
                            decimal unitPrice = (decimal)(dr["UnitPrice"]);
                            string categoryName = (string)(dr["CategoryName"]);
                            string CompanyName = (string)(dr["CompanyName"]);
                            Console.WriteLine(
                            $"[{productID,4}] {productName,40} - {unitsInStock,5} | {unitPrice,10:C} | {categoryName,10} | {CompanyName,10}");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }

    }
    public static void DisplaySupplierProductCounts()
    {

        try
        {
            using (SqlConnection cn = new SqlConnection(getConnectionString()))
            {
                cn.Open();
                string sql =
                "Select Products.SupplierID, count(*) as suppc " +
                "From Products JOIN Categories on Products.CategoryID =Categories.CategoryID " +
                "JOIN Suppliers on Suppliers.SupplierID = Products.SupplierID GROUP BY Products.SupplierID";
                //+
                //"GROUP BY Products.SupplierID";
                //+ " where CompanyName = '" + inputSupplierName + "'";
                using (SqlCommand cmd = new SqlCommand(sql, cn))
                {
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {

                            //string CompanyName = (string)(dr["CompanyName"]);
                            int SupplierID = (int)(dr["SupplierID"]);
                            int count = (int)(dr["suppc"]);
                            Console.WriteLine(
                            $"[{SupplierID,4}] {count,40}");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    public static void DisplaySupplierProductCounts2()
    {

        try
        {
            using (SqlConnection cn = new SqlConnection(getConnectionString()))
            {
                cn.Open();
                string sql =
                "Select Suppliers.CompanyName, count(*) as suppc " +
                "From Products JOIN Categories on Products.CategoryID =Categories.CategoryID " +
                "JOIN Suppliers on Suppliers.SupplierID = Products.SupplierID " +
                "GROUP BY Suppliers.CompanyName";
                //+
                //"GROUP BY Products.SupplierID";
                //+ " where CompanyName = '" + inputSupplierName + "'";
                using (SqlCommand cmd = new SqlCommand(sql, cn))
                {
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {

                            string CompanyName = (string)(dr["CompanyName"]);
                            //int SupplierID = (int)(dr["SupplierID"]);
                            int count = (int)(dr["suppc"]);
                            Console.WriteLine(
                            $"[{CompanyName,40}] {count,4}");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
    public static void DisplayCustomersandSuppliersByCity()
    {

        //Customer and Suppliers by City
        try
        {
            using (SqlConnection cn = new SqlConnection(getConnectionString()))
            {
                cn.Open();
                string sql =
                "Select * FROM [dbo].[Customer and Suppliers by City]";
                //+
                //"From Product ";
                using (SqlCommand cmd = new SqlCommand(sql, cn))
                {
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            //City, CompanyName, ContactName, 'Customers' AS Relationship
                            string City = (string)(dr["City"]);
                            string CompanyName = (string)dr["CompanyName"];
                            string ContactName = (string)(dr["ContactName"]);
                            string Relationship = (string)(dr["Relationship"]);
                            Console.WriteLine(
                            $"{City,18} | {CompanyName,39} - {ContactName,28} | {Relationship,15}");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }

    }

    public static void DisplayTenMostExpensiveProducts()
    {
        try
        {
            using (SqlConnection cn = new SqlConnection(getConnectionString()))
            {
                cn.Open();
                string sql = "Ten Most Expensive Products";
                using (SqlCommand cmd = new SqlCommand(sql, cn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            string productName =
  (string)dr["TenMostExpensiveProducts"];
                            decimal unitPrice = (decimal)dr["UnitPrice"];
                            Console.WriteLine($"{productName,30} -- {unitPrice,10:C}");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }

    }

    public static void DisplaySalesByCategory(string catName, string year)
    {
        try
        {
            using (SqlConnection cn = new SqlConnection(getConnectionString()))
            {
                cn.Open();
                string sql = "SalesByCategory";
                using (SqlCommand cmd = new SqlCommand(sql, cn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("CategoryName", catName);
                    cmd.Parameters.AddWithValue("OrdYear", year);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {

                        //                       ProductName,
                        //TotalPurchase
                        while (dr.Read())
                        {
                            string productName =
  (string)dr["ProductName"];
                            decimal TotalPurchase = (decimal)dr["TotalPurchase"];
                            Console.WriteLine($"{productName,30} -- {TotalPurchase,10:C}");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }

    }

    public static void DisplayCategoryCount()
    {
        //string? connStr = getConnectionString();
        SqlConnection cn = new SqlConnection(getConnectionString());
        cn.Open();
        string sql = "Select Count(*) From Categories";
        SqlCommand cmd = new SqlCommand(sql, cn);
        int categoriesCount = (int)cmd.ExecuteScalar();
        Console.WriteLine($"We have {categoriesCount} Categories");


    }
    public static void DisplayTotalInventoryValue()
    {
        SqlConnection cn = new SqlConnection(getConnectionString());
        cn.Open();
        string sql = "SELECT SUM(UnitsInStock * UnitPrice) FROM Products";
        SqlCommand cmd = new SqlCommand(sql, cn);
        decimal TotalInventoryValue = (decimal)cmd.ExecuteScalar();
        Console.WriteLine($"TotalInventoryValue: {TotalInventoryValue} ");
    }
    public static void AddAShipper()
    {
        SqlConnection cn = new SqlConnection(getConnectionString());
        cn.Open();
        string sql = "INSERT INTO Shippers VALUES ('New Shipper',3134441122)";
        SqlCommand cmd = new SqlCommand(sql, cn);
        int rowsChanged = cmd.ExecuteNonQuery();
        Console.WriteLine($"{rowsChanged} rows changed");
    }

    public static void DisplayShippers()
    {

        SqlConnection cn = new SqlConnection(getConnectionString());
        cn.Open();
        string sql = "Select ShipperID, CompanyName From Shippers";
        SqlCommand cmd = new SqlCommand(sql, cn);

        SqlDataReader dr = cmd.ExecuteReader();
        while (dr.Read())
        {
            int ShipperID = (int)dr["ShipperID"];
            string productName = (string)dr["CompanyName"];

            Console.WriteLine(
            $"[{ShipperID}] {productName}");
        }
        dr.Close();
    }
    public static void ChangeShipperName()
    {

        SqlConnection cn = new SqlConnection(getConnectionString());
        cn.Open();
        string sql =
"Update Shippers Set CompanyName = 'Updated New Shipper' " +
"Where CompanyName = 'New Shipper'";
        SqlCommand cmd = new SqlCommand(sql, cn);
        int rowsChanged = cmd.ExecuteNonQuery();
        Console.WriteLine($"{rowsChanged} rows changed");

    }

    public static void DeleteShipper()
    {
        SqlConnection cn = new SqlConnection(getConnectionString());
        cn.Open();
        string sql =
"DELETE FROM Shippers WHERE CompanyName = 'Updated New Shipper' OR CompanyName = 'New Shipper'";
        SqlCommand cmd = new SqlCommand(sql, cn);
        int rowsChanged = cmd.ExecuteNonQuery();
        Console.WriteLine($"{rowsChanged} rows changed");
    }


    public static string getConnectionString()
    {
        ConfigurationBuilder builder = new ConfigurationBuilder();
        builder.SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", optional: false,
        reloadOnChange: true);
        IConfiguration config = builder.Build();
        string? connStr = config["ConnectionStrings:Northwind"];
        return connStr;
    }
}