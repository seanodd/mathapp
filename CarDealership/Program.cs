﻿using System;
using System.IO;

namespace CarDealership;
class Program
{
    static void Main(string[] args)
    {

        string[,] cars = new string[5, 6];
        processCarData(ref cars);

        DisplayCarMatrix(ref cars);

        UserPromptCarDetails(ref cars);

    }

    private static void UserPromptCarDetails(ref string[,] cars)
    {
        int carId = 0;
        string command = "";
        Console.Write("Which car are you interested in (enter #1-5)? ");
        try
        {
            carId = Convert.ToInt32(Console.ReadLine());
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
            //continue;
        }
        for (int j = 0; j < cars.GetLength(1); j++)
        {

            Console.Write("{0,10}", cars[carId-1, j]);

        }
        Console.Write(Environment.NewLine);
        Console.WriteLine("Type BUY to buy this car or MAIN to return to the main menu? ");
        try
        {
            command = Console.ReadLine();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
            //continue;
        }
        if (command.ToUpper() == "BUY")
        {

            //string[] car = cars[carId-1];
            Console.WriteLine("You have selected the {0} {1} to purchase.", cars[carId - 1, 1], cars[carId - 1, 2]);
            Console.WriteLine("Generating Invoice.");
            GenerateInvoice(ref cars, carId);
        }

    }



    public static void processCarData(ref string[,] cars)
    {
        string fileName = @"C:\Academy\repos\MathApp\CarDealership\data\cars.csv";
        StreamReader inputFile = null;
        //string[,] cars = new string[5, 6];
        //string[] car;
        int carCounter = 0;

        try
        {
            inputFile = new StreamReader(fileName);
            while (!inputFile.EndOfStream)
            {
                string line = inputFile.ReadLine();
                string[] car = line.Split(',');
                //car = ProcessLine(line);
                for (int i = 0; i < car.Length; i++)
                {
                    cars[carCounter, i] = car[i];
                }
                carCounter++;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(
            $"Error opening file: {ex.Message}");
        }
        finally
        {
            if (inputFile != null) inputFile.Close();
        }
    }
    public static void DisplayCarMatrix(ref string[,] cars)
    {
        // TOP ROW of NUMBERs 
        Console.Write("{0,4}", "");
        for (int k = 0; k < cars.GetLength(1); k++)
        {
            Console.Write("{0,10}", k);
        }
        Console.Write(Environment.NewLine);
        Console.Write("{0,4}", "");

        // TOP ROW of dashes 
        for (int m = 0; m < cars.GetLength(1); m++)
        {
            Console.Write("{0,10}", "---");
        }
        Console.Write(Environment.NewLine);
        for (int i = 0; i < cars.GetLength(0); i++)
        {
            Console.Write($"{i} | ");
            for (int j = 0; j < cars.GetLength(1); j++)
            {

                Console.Write("{0,10}", cars[i, j]);
            }
            Console.Write(Environment.NewLine + Environment.NewLine);
        }
    }

    public static string[] ProcessLine(string line)
    {
        Console.WriteLine(line);
        //String[] car;
        String[] car = line.Split(',');
        return car;
        //double individualGrossPay = Convert.ToDouble(fields[2]) * Convert.ToDouble(fields[3]);
        //Console.WriteLine($"{fields[1]} | {individualGrossPay} ");
        //return individualGrossPay;


    }

    public static void GenerateInvoice(ref string[,] cars, int carId)
    {
        string time = DateTime.Now.ToString("MM_dd_yyyy_hh_mm_ss");
        string outfileName = @"C:\Academy\repos\MathApp\CarDealership\invoice\car_invoice_" + time + ".txt";
        StreamWriter outputFile = new StreamWriter(outfileName, true);

        outputFile.WriteLine("Invoice for _____ On {0:MM-dd-yyyy} at {0:hh:mm:ss tt}", DateTime.Now);
        outputFile.WriteLine("{0} {1}", cars[carId - 1, 1], cars[carId - 1, 2]);
        outputFile.WriteLine("----");
        outputFile.Close();

    }
}

///SAMPE FOR INIT
///
//string[] lines = File.ReadAllLines("C:/temp/test.txt");

//int sizex = lines.Length;
//int sizey = 1;
//        for (int i = 0; i<lines.Length; i++)
//        {
//            var splitline = lines[i].Split(' ');
//sizey = sizey<splitline.Length? splitline.Length : sizey;
//        }

//        String[,] multillines = new string[sizex, sizey];

//for (int i = 0; i < lines.Length; i++)
//{
//    var splitline = lines[i].Split(' ');
//    for (int j = 0; j < splitline.Length; j++)
//    {
//        multillines[i, j] = splitline[j];
//    }
//}