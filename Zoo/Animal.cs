﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{

    public abstract class Animal
    {
        public Animal(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public string Name { get; set; }
        public int Age { get; set; }
        //public Animal(string owner, string descry, decimal cost)
        //{
        //    Owner = owner;
        //    Description = descry;
        //    OriginalCost = cost;
        //}
        public abstract string MakeSound();

        public void DisplayInfo()
        {
            Console.WriteLine("Animal - name: " + this.Name + " | age: " + this.Age + " | sound: " + this.MakeSound());
        }
    }
}
