﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    public class Lion : Animal, ICarnivore
    {
        public Lion(string name, int age) : base(name, age)
        {
        }

        public string Hunt()
        {
            return "The lion stalks its prey and pounces to catch it.";
        }

        public override string MakeSound()
        {
            return "Roar";
        }
    }
}
