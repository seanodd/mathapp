﻿namespace Zoo;

class Program
{
    static void Main(string[] args)
    {
        // NEED TO UPDATE CAR VALUE Depreciation and LOOP FOR Stock
        Animal[] animals = new Animal[3];
        animals[0] = new Lion("Simba", 5);

        animals[1] = new Elephant("Dumbo", 20);

        animals[2] = new Giraffe("Leroy", 14);
            

        //assets[3] = new Car(1000001, "Jeep Grand Cherokee", new DateTime(2022, 10, 10), 47000.00, "2022", 5000.00);
        //assets[4] = new Car(1000002, "Ford Explorer", new DateTime(2020, 02, 10), 38000.00, "2020", 20000.00);
        //assets[5] = new Car(1000003, "Porsche Carrera ", new DateTime(2023, 03, 10), 106000, "2023", 500);


        foreach (Animal a in animals)
        {
            Console.WriteLine($"{a.Name,25 } | {a.Age,25 } | {a.MakeSound(),8}");
            a.DisplayInfo();
            if (a is ICarnivore)
            {
                Console.WriteLine(((ICarnivore)a).Hunt());
            }
            if (a is IHerbivore)
            {
                Console.WriteLine(((IHerbivore )a).Graze());
            }
        }
        //Console.WriteLine("------------------  CARS  -----------------");
        //foreach (Asset a in assets)
        //{
        //    Car c = null;
        //    if (a is Car)
        //    {
        //        c = (Car)a;
        //    }

        //    if (c != null)
        //    {
        //        Console.WriteLine($"{c.Description,25 } | {c.DateAcquired,25 } | {c.ModelYear,8} | {c.OdometerReading}");
        //    }

        //}

    }
}