﻿


namespace Searcher;
class Program
{
    static void Main(string[] args)
    {
        string command = "";
        do
        {
            string name;
            string itemName;


            string[] items = { "Sausage Breakfast Taco",
"Potato and Egg Breakfast Taco", "Sausage and Egg Biscuit",
"Bacon and Egg Biscuit", "Pancakes"};
            decimal[] prices = { 3.99M, 3.29M, 3.70M, 3.99M, 4.79M };
            decimal? price = null;

            items.ToList().ForEach(i => Console.WriteLine(i.ToString()));
            Console.Write("What do you want to order? ");

            try
            {
                itemName = Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                continue;
            }

            //GetData(in major_code, in classification, out major, out location);
            price = GetPrice(ref items,ref prices, itemName);

            if (price.HasValue)
            {
                Console.WriteLine("Item: {0} | Price: {1} ", itemName, price);
            }
            else
            {
                Console.WriteLine("That item is not on the menu.");
            }
                


            Console.Write("Do you want to enter another ? (Y / N)? ");
            command = Console.ReadLine();
        } while (command != "N");
    }

    static decimal? GetPrice(
        ref string[] items,
        ref decimal[] prices,
        in string itemName 
        )
    {

        decimal? price = null;

        for (int i = 0; i < items.Length; i++)
        {
            if (items[i] == itemName)
            {
                price = prices[i];
                break;
            }
        }
        return price;
    }


        //STILL TO DO: What if bad classification
        static void GetData(
        in string major_code,
        in string classification,
        out string major,
        out string location)
    {

        major = "";
        location = "";
        switch (major_code)
        {

            //Major Name of Major
            //Code
            //BIOL
            //Biology
            //CSCI
            //Computer Science
            //Sheppard Hall, Room 314
            //ENG
            //English
            //HIST
            //History
            //MKT
            //Marketing
            //Department Office
            //Freshman - Sophomore: Science Bldg, Room 310 Junior - Senior: Science Bldg, Room 311
            //Freshmen: Kerr Hall, Room 201 Sophomore - Senior: Kerr Hall, Room 312 Kerr Hall, Room 114
            //Freshman - Junior: Westly Hall, Room 310 Senior: Westly Hall, Room 313

            case "BIOL":
                major = "Biology";
                //Freshman - Sophomore: Science Bldg, Room 310 Junior - Senior: Science Bldg, Room 311
                switch (classification)
                {
                    case string i when classification == "Freshman" || classification == "Sophomore":
                        location = "Science Bldg, Room 310";
                        break;
                    case string i when classification == "Junior" || classification == "Senior":
                        location = "Science Bldg, Room 311";
                        break;
                    default:
                        break;
                }
                break;
            case "CSCI":
                major = "Computer Science";
                switch (classification)
                {
                    default:
                        location = "Sheppard Hall, Room 314";
                        break;
                }
                break;
            case "ENG":
                major = "English";
                //Freshmen: Kerr Hall, Room 201 Sophomore - Senior: Kerr Hall, Room 312 Kerr Hall, Room 114
                switch (classification)
                {
                    case string i when classification == "Freshman":
                        location = "Kerr Hall, Room 201";
                        break;
                    case string i when classification == "Sophomore" || classification == "Junior" || classification == "Senior":
                        location = "Kerr Hall, Room 312";
                        break;
                    default:
                        break;
                }
                break;
            case "HIST":
                major = "History";
                switch (classification)
                {
                    default:
                        location = "Kerr Hall, Room 114";
                        break;
                }
                break;
            case "MKT":
                major = "Marketing";
                //Freshman - Junior: Westly Hall, Room 310 Senior: Westly Hall, Room 313
                switch (classification)
                {
                    case string i when classification == "Senior":

                        location = "Westly Hall, Room 313";
                        break;
                    default:
                        location = "Westly Hall, Room 310";
                        break;
                }
                break;
            default:
                break;
        }
    }
}




