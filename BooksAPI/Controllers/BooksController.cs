﻿using BooksAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BooksAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly BookEntities _context;
        public BooksController(BookEntities context)
        {
            _context = context;
        }

        //private readonly List<Book> _books =
        //     new List<Book>
        //     {
        //     // Seeding data that would really live in
        //     // a database somewhere
        //     new Book { Author = "JK Rowling", BookId = 1, Price = 19.99, Publisher="Kids Books", Title = "Harry Potter", YearPublished = 1999},
        //    new Book { Author = "Dan Brown", BookId = 2, Price = 17.89, Publisher="Mystery Books", Title = "Da Vinci Code", YearPublished = 2002}


        //         };


        //// GET: api/<BooksController>
        //[HttpGet]
        //public IEnumerable<Book> Get()
        //{
        //    return _books;
        //}

        // GET: api/Books
        [HttpGet]
        public IEnumerable<Book> Get()
        {

            //var context = new BookEntities();

            return _context.Books;
        }

        // GET: api/Books/5
        [HttpGet("{id}")]
        public Book Get(int id)
        {
            Book book = null;
            //using (var context = new BookEntities())
            //{
            book = _context.Books
            .Where(b => b.BookId == id)
            .SingleOrDefault();

            return book;
        }

        //// GET api/<BooksController>/5
        //[HttpGet("{id}")]
        //public ActionResult<Book> Get(int id)
        //{
        //    Book book = _books.FirstOrDefault(b => b.BookId == id);
        //    if (book == null)
        //    {
        //        return NotFound();
        //    }
        //    return book;
        //}

        //// POST api/<BooksController>
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        [HttpPost]
        public ActionResult Post(Book book)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //book.BookId = _books.Count + 1;
            _context.Books.Add(book);
            _context.SaveChanges();
            string location = $"api/books/{book.BookId }";
            return Created(location, book);
        }

        // PUT api/<BooksController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, Book book)
        {
            var existingBook =
             _context.Books.FirstOrDefault(c => c.BookId == id);
            // This implementation returns a 404 if the resource
            // is not found
            if (existingBook == null)
            {
                return NotFound();
            }
            else
            {
                existingBook.Title = book.Title;
                existingBook.Author = book.Author;
                existingBook.Publisher = book.Publisher;
                existingBook.YearPublished = book.YearPublished;
                existingBook.Price = book.Price;
                _context.SaveChanges();

                return Ok();
            }
        }

        // DELETE api/<BooksController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var existingBook = _context.Books.FirstOrDefault(c => c.BookId == id);
            if (existingBook == null)
            {
                return NotFound();
            }

            _context.Books.Remove(existingBook);
            _context.SaveChanges();
            return Ok();
        }
    }
}
