﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooksAPI.Models;
using Microsoft.EntityFrameworkCore;


namespace BooksAPI.Models
{
    public class BookEntities : DbContext
    {

        public BookEntities(DbContextOptions<BookEntities> options) : base(options)
        {
        }

        public DbSet<Book> Books { get; set; }
        //public DbSet<Category> Categories { get; set; }

        //public virtual DbSet<GetMoviesByCategory> GetMoviesByCategory { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=BooksDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        }

        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        //        => optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Northwind;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

        protected override void OnModelCreating(
        ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>()
                .HasKey(x => x.BookId);
            //.HasMany(category => category.Movies)
            //.WithOne(movie => movie.Category);
            ////.HasForeignKey(movie => .AuthorID);

            //modelBuilder.Entity<GetMoviesByCategory>(entity =>
            //{
            //    entity
            //    .HasNoKey();
            //});

            modelBuilder.Entity<Book>().HasData(
             new Book { Author = "JK Rowling", BookId = 1, Price = 19.99, Publisher = "Kids Books", Title = "Harry Potter", YearPublished = 1999 },
            new Book { Author = "Dan Brown", BookId = 2, Price = 17.89, Publisher = "Mystery Books", Title = "Da Vinci Code", YearPublished = 2002 }


            );

        }
    }
}





