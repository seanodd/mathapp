﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AnimalShelterAPI.Models
{
    public class AnimalManager
    {
        private readonly AnimalShelterContext animalShelterContext;
        public AnimalManager(AnimalShelterContext animalShelterContext)
        {
            this.animalShelterContext = animalShelterContext;
        }

        public async Task<IEnumerable<Animal>> GetAnimals()
        {
            return await animalShelterContext.Animals.ToListAsync();

        }



        //////////////////
        //////This returns one, needs to be updated to return list
        public async Task<Animal> GetByOwner(string owner)
        {
            return await animalShelterContext.Animals
                .FirstOrDefaultAsync(a => a.Owner == owner);
        }

        //[HttpGet]
        //public IEnumerable<Animal> GetAllAnimals()
        //{
        //List<Animal> animals = null;
        //using (var context = new AnimalShelterContext())
        //{
        //animals = context.Animals.ToList();
        //}
        //return animals;
        //}
        //[HttpGet("{id}")]
        //public Animal GetAnimalById(string id)
        //{
        //Animal animal = null;
        //using (var context = new AnimalShelterContext())
        //{
        //animal = context.Animals
        //.Where(c => c.AnimalID == id)
        //.SingleOrDefault();
        //}
        //return animal;
        //}
    }
}
