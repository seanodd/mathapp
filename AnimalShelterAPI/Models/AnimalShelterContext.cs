﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnimalShelterAPI.Models;
using Microsoft.EntityFrameworkCore;


namespace AnimalShelterAPI.Models
{
    public class AnimalShelterContext : DbContext
    {

        public AnimalShelterContext(DbContextOptions<AnimalShelterContext> options) : base(options)
        {
        }

        public DbSet<Animal> Animals { get; set; }
        //public DbSet<Category> Categories { get; set; }

        //public virtual DbSet<GetMoviesByCategory> GetMoviesByCategory { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=AnimalShelterDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        }

        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        //        => optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Northwind;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

        protected override void OnModelCreating(
        ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Animal>()
                .HasKey(x => x.AnimalId);
            //.HasMany(category => category.Movies)
            //.WithOne(movie => movie.Category);
            ////.HasForeignKey(movie => .AuthorID);

            //modelBuilder.Entity<GetMoviesByCategory>(entity =>
            //{
            //    entity
            //    .HasNoKey();
            //});

            modelBuilder.Entity<Animal>().HasData(
             new Animal { AnimalId = 1, Breed = "Maine Coon", Name = "Ziggy", Owner = "Emma", Species = "Cat", CheckInDate = new DateTime(2023,4,5), CheckOutDate = new DateTime(2023, 4, 9), },
             new Animal { AnimalId = 2, Breed = "Tabby", Name = "Orangie", Owner = "Sean", Species = "Cat", CheckInDate = new DateTime(2023, 3, 5), CheckOutDate = new DateTime(2023, 4, 9), }


            );

    }
}
}





