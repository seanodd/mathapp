﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileProcessing
{
    public class TimeCard
    {

        public string Name { get; set; }
        public double HoursWorked;
        public double PayRate;

        public TimeCard(string name, double hoursWorked, double payRate)
        {
            Name = name;
            HoursWorked = hoursWorked;
            PayRate = payRate;
        }

        public static TimeCard CreateTimeCard(string data)
        {
            string[] fields = data.Split('|');
            TimeCard timeCard = null;
            try
            {
                timeCard = new TimeCard(fields[0], Convert.ToDouble(fields[1]), Convert.ToDouble(fields[2]));
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine($"Incorrect file format, could not parse: {ex.Message}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
            return timeCard;
        }

        public double GetGrossPay()
        {
            //Console.WriteLine("--- Stock - GetValue for " + this.Description + ": " + this.OriginalCost.ToString());
            return this.HoursWorked * this.PayRate;
        }
    }
}
