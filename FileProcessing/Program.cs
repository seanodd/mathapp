﻿namespace FileProcessing;

using System;
using System.IO;
class Program
{
    static void Main(string[] args)
    {
        string fileName = @"C:\Academy\repos\MathApp\FileProcessing\data\PayrollData1.txt";
        string fileName2 = @"C:\Academy\repos\MathApp\FileProcessing\data\PayrollData2.txt";
        string fileName3 = @"C:\Academy\repos\MathApp\FileProcessing\data\PayrollData3.txt";

        string[] files = { fileName, fileName2, fileName3 };
        foreach (string file in files)
        {
            StreamReader inputFile = null;
            double totalGrossPay = 0.0;
            double individualGrossPay = 0.0;
            try
            {
                inputFile = new StreamReader(file);
                while (!inputFile.EndOfStream)
                {
                    string text = inputFile.ReadLine();
                    //individualGrossPay = ProcessLine(text);
                    TimeCard tc = TimeCard.CreateTimeCard(text);
                    //Console.WriteLine($"Gross Pay: { tc.GetGrossPay()} ");
                    if (tc != null)
                    {
                        Console.WriteLine($"{ tc.Name } | { tc.GetGrossPay() } ");
                        totalGrossPay = totalGrossPay + tc.GetGrossPay();
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(
                $"Payroll data file not found : {ex.Message}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(
                $"Error opening file: {ex.Message}");
            }
            finally
            {
                if (inputFile != null) inputFile.Close();
            }
            Console.WriteLine($"Total Gross Pay: {totalGrossPay} ");

            // Writing to file 
            string outfileName = @"C:\Academy\repos\MathApp\FileProcessing\log\PayrollFileProcessor.log";
            StreamWriter outputFile =
            new StreamWriter(outfileName, true);

            outputFile.WriteLine("Processing file: {1} On {0:MM-dd-yyyy} at {0:hh:mm:ss tt}", DateTime.Now, file);
            outputFile.WriteLine("Gross pay totals were ${0}", totalGrossPay);
            outputFile.WriteLine("----");
            outputFile.Close();

        }
        //
    }
    //public static double ProcessLine(string line)
    //{
    //    //Console.WriteLine(line);
    //    String[] fields = line.Split('|');
    //    double individualGrossPay = Convert.ToDouble(fields[1]) * Convert.ToDouble(fields[2]);
    //    Console.WriteLine($"{fields[0]} | {individualGrossPay} ");
    //    return individualGrossPay;


    //}
}