﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassPlay
{
    internal class Employee
    {
        // OLD WITHOUT CHAINING
        //public Employee(string firstName, string lastName, int id)
        //{
        //    FirstName = firstName;
        //    LastName = lastName;
        //    Id = id;
        //    StartDate = DateTime.Now;
        //    StartYear = DateTime.Now.Year;
        //    Title = "TBD";
        //    Salary = 31200;
        //}
        public Employee(string firstName, string lastName, int id) : this(firstName, lastName, id, DateTime.Now, "TBD", 31200)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Id = id;
            Console.WriteLine("Creating Employee using 2nd Contructor");
        }

        public Employee(string firstName, string lastName, int id, DateTime startDate, string title, int salary)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Id = id;
            this.StartDate = startDate;
            this.StartYear = startDate.Year;
            this.Title = title;
            this.Salary = salary;
            Console.WriteLine("Creating Employee using 1st Contructor");
        }


        public void Promote(string title, int salary)
        {
            this.Title = title;
            this.Salary = salary;
            Console.WriteLine("Promoting " + this.FirstName + " " + this.LastName);
        }

        public void Display()
        {
            Console.WriteLine("First Name   : " + this.FirstName);
            Console.WriteLine("Last Name    : " + this.LastName);
            Console.WriteLine("Employee ID  : " + this.Id);
            Console.WriteLine("Start Date   : " + this.StartDate);
            Console.WriteLine("Start Year   : " + this.StartYear);
            Console.WriteLine("Title        : " + this.Title);
            Console.WriteLine("Salary       : " + this.Salary);
        }

        // properties
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public int StartYear { get; set; }
        public string Title { get; set; }
        public int Salary { get; set; }

    }

}
