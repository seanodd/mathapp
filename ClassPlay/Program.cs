﻿namespace ClassPlay
{

    class Program
    {
        static void Main(string[] args)
        {
            Employee e = new Employee("Dana", "Lynn", 1);
            //DisplayEmployee(e);
            e.Display();

            Employee e2 = new Employee("Indiana", "Jones", 2, new DateTime(1950,01,01), "Professor", 50000);
            //DisplayEmployee(e2);
            e2.Display();

            Employee e3 = new Employee("Luke","Sky",3)
            {
                StartDate = new DateTime(1987, 01, 01),
                StartYear = new DateTime(1987, 01, 01).Year,
                Title = "Farmer",
                Salary = 80000
            };
            //DisplayEmployee(e3);
            e3.Display();

            e.Promote("Manager", 62000);
            e.Display();

        }

        //public static void DisplayEmployee(Employee e)
        //{
        //    Console.WriteLine("First Name   : " + e.FirstName);
        //    Console.WriteLine("Last Name    : " + e.LastName);
        //    Console.WriteLine("Employee ID  : " + e.Id);
        //    Console.WriteLine("Start Date   : " + e.StartDate);
        //    Console.WriteLine("Start Year   : " + e.StartYear);
        //    Console.WriteLine("Title        : " + e.Title);
        //    Console.WriteLine("Salary       : " + e.Salary);
        //}
    }
}
