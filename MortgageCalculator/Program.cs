﻿
namespace MortgageCalculator;
class Program
{
    static void Main(string[] args)
    {
        string command = "";
        do
        {
            double borrowing = 0.0;
            double interest_rate = 0.0;
            double loan_length = 0.0;
            double MIR;
            double np;

            Console.Write("How much are you borrowing: ");

            try
            {
                borrowing = Convert.ToDouble(Console.ReadLine());
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                continue;
            }
            Console.Write("What is your interest rate ? ");
          
            try
            {
                interest_rate = Convert.ToDouble(Console.ReadLine());
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
            Console.Write("How long is your loan (in years)? ");

            try
            {
                loan_length = Convert.ToDouble(Console.ReadLine());
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }

            MIR = interest_rate / 1200.00;
            np = loan_length * 12.0 * -1.0;
            double top = borrowing * MIR;
            double bot = 1.0 - Math.Pow((1.0 + MIR), np);
            double est = top / bot;
            double total = est * (loan_length * 12.0);
            double total_interest = total - borrowing;
            //Console.WriteLine($"estimated_payment: {est}");
            //Console.WriteLine($"You paid {total} over the life of the loan");
            //Console.WriteLine($"Your total interest cost for the loan was {total_interest}");

            Console.WriteLine("estimated_payment: {0:C}", est);
            Console.WriteLine("You paid {0:C} over the life of the loan", total);
            Console.WriteLine("Your total interest cost for the loan was {0:C}", total_interest);

        
        Console.Write("Do you want to get information for another loan (Y / N)? ");
            command = Console.ReadLine();
        } while (command != "N");
    }
}







