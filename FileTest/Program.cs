﻿
string path = @"C:\Academy\repos\MathApp\FileTest";
// Make sure the directory does exist
if (!Directory.Exists(path))
{
    Console.WriteLine($"Error: {path} doesn't exist!");
    return;
}
string[] fileNames = Directory.GetFiles(path);
foreach (string name in fileNames)
{
    Console.WriteLine(name);
}

string testpath = @"C:\Academy\repos\MathApp\FileTest\testing";
// Check to see if the directory already exists
if (Directory.Exists(testpath))
{
    Console.WriteLine(
    "Error: Can't create {0} because already exists!",
    testpath);
    return;
}
// Create the directory
Directory.CreateDirectory(testpath);