﻿

namespace NameParser;
class Program
{
    static void Main(string[] args)
    {
        string command = "";
        do
        {
            string name;
            string firstName = "";
            string lastName = "";
            string middleName = "";
            string major_code = "";
            string major = "";
            string classification = "";
            string location = "";


            Console.Write("What is the name? ");

            try
            {
                name = Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                continue;
            }


            //GetData(in major_code, in classification, out major, out location);
            ParseName(
                in name,
                out firstName,
                out middleName,
                out lastName);

            Console.WriteLine("First name : " + firstName);
            Console.WriteLine("Middle name: " + middleName);


            Console.WriteLine("Last name  : " + lastName);


            Console.Write("Do you want to enter another ? (Y / N)? ");
            command = Console.ReadLine();
        } while (command != "N");
    }

    static void ParseName(
       in string full_name,
        out string firstName,
        out string middleName,
        out string lastName)
    {
        firstName = "";
        lastName = "";
        middleName = "";

        int pos = full_name.IndexOf(" ");
        int pos2 = full_name.LastIndexOf(" ");
        //Console.WriteLine("pos        : " + pos);
        //Console.WriteLine("pos2       : " + pos2);

        if (pos < 0)
        {
            firstName = full_name;
            Console.WriteLine("Only name  : " + full_name);
        }
        else
        {
            firstName = full_name.Substring(0, pos);
            lastName = full_name.Substring(pos + 1);
            Console.WriteLine("First name : " + firstName);
            if (pos2 > 0 && pos2 != pos)
            {
                middleName = full_name.Substring(pos + 1, pos2 - pos);
                lastName = full_name.Substring(pos2 + 1);
                Console.WriteLine("Middle name: " + middleName);
            }

            Console.WriteLine("Last name  : " + lastName);
        }
    }
}



