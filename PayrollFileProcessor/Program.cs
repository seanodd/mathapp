﻿using System;
using System.IO;
class Program
{
    static void Main(string[] args)
    {
        string fileName = @"C:\Academy\repos\MathApp\PayrollFileProcessor\data\Payroll1.txt";
        StreamReader inputFile = null;
        double totalGrossPay =0.0;
        double individualGrossPay = 0.0;
        try
        {
            inputFile = new StreamReader(fileName);
            while (!inputFile.EndOfStream)
            {
                string text = inputFile.ReadLine();
                individualGrossPay = ProcessLine(text);
                totalGrossPay = totalGrossPay + individualGrossPay;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(
            $"Error opening file: {ex.Message}");
        }
        finally
        {
            if (inputFile != null) inputFile.Close();
        }
        Console.WriteLine($"Total Gross Pay: {totalGrossPay} ");

        // Writing to file 
        string outfileName = @"C:\Academy\repos\MathApp\PayrollFileProcessor\log\PayrollFileProcessor.log";
        StreamWriter outputFile =
        new StreamWriter(outfileName, true);

        outputFile.WriteLine("Processing file: Payroll1.txt On {0:MM-dd-yyyy} at {0:hh:mm:ss tt}", DateTime.Now);
        outputFile.WriteLine("Gross pay totals were ${0}", totalGrossPay);
        outputFile.WriteLine("----");      
        outputFile.Close();
        //
    }
public static double ProcessLine(string line)
    {
        //Console.WriteLine(line);
        String[] fields = line.Split(',');
        double individualGrossPay = Convert.ToDouble(fields[2]) * Convert.ToDouble(fields[3]);
        Console.WriteLine($"{fields[1]} | {individualGrossPay} ");
        return individualGrossPay;


    }
}