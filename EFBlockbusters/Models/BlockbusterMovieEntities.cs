﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace EFBlockbusters.Models
{
    public class BlockbusterMovieEntities : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Category> Categories { get; set; }

        public virtual DbSet<GetMoviesByCategory> GetMoviesByCategory { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=BlockbusterMoviesDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        }

        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        //        => optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Northwind;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

        protected override void OnModelCreating(
        ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
            .HasMany(category => category.Movies)
            .WithOne(movie => movie.Category);
            //.HasForeignKey(movie => .AuthorID);

            modelBuilder.Entity<GetMoviesByCategory>(entity =>
            {
                entity
                .HasNoKey();
            });

            modelBuilder.Entity<Category>().HasData(
            new Category
            {
                CategoryID = 1,
                CategoryName = "Romantic Comedy"
            },
            new Category
            {
                CategoryID = 2,
                CategoryName = "Action"
            },
            new Category
            {
                CategoryID = 3,
                CategoryName = "Kids"
            },
            new Category
            {
                CategoryID = 4,
                CategoryName = "Science Fiction"
            },
            new Category
            {
                CategoryID = 5,
                CategoryName = "Family"
            },
            new Category
            {
                CategoryID = 6,
                CategoryName = "Western"
            }
            );
            modelBuilder.Entity<Movie>().HasData(
                new {
                           Title = "2001: A Space Odyssey",
                           MovieID = 1,
                           Description = "When Dr. Dave Bowman (Keir Dullea) and other astronauts are sent on a mysterious mission, their ship's computer system, HAL, begins to display increasingly strange behavior.",
                           CategoryID = 4
                 },
                new
                {
                    Title = "Star Wars: Episode IV - A New Hope",
                    MovieID = 2,
                    Description = "Movie about Luke Skywalker",
                    CategoryID = 4
                },
                new
                {
                    Title = "Blade Runner",
                    MovieID = 3,
                    Description = "Movie about Blade Runners",
                    CategoryID = 4
                },
                new
                {
                    Title = "The Matrix",
                    MovieID = 4,
                    Description = "Movie about Neo",
                    CategoryID = 4
                },
                new
                {
                    Title = "The Dark Knight",
                    MovieID = 5,
                    Description = "Movie about batman",
                    CategoryID = 4
                },
                new
                {
                    Title = "Mad Max",
                    MovieID = 6,
                    Description = "Movie about Max.",
                    CategoryID = 2
                },
                new
                {
                    Title = "Aliens",
                    MovieID = 7,
                    Description = "Movie about Aliens",
                    CategoryID = 2
                },
                new
                {
                    Title = "Die Hard",
                    MovieID = 8,
                    Description = "Movie about NYC police officer saving hostages from building",
                    CategoryID = 2
                },
                new
                {
                    Title = "Terminator 2: Judgment Day",
                    MovieID = 9,
                    Description = "Movie about Terminators",
                    CategoryID = 2
                },
                new
                {
                    Title = "Casino Royale",
                    MovieID = 10,
                    Description = "Movie about James bond",
                    CategoryID = 2
                },
///////////////////////////////////////
                new
                {
                    Title = "Notting Hill",
                    MovieID = 11,
                    Description = "Movie with Hugh Grant",
                    CategoryID = 1
                },
                new
                {
                    Title = "When Harry Met Sally",
                    MovieID = 12,
                    Description = "Movie about Harry",
                    CategoryID = 1
                },
                new
                {
                    Title = "Beauty and the Beast",
                    MovieID = 13,
                    Description = "Movie about a Beast",
                    CategoryID = 3
                },
                new
                {
                    Title = "Toy Story",
                    MovieID = 14,
                    Description = "Story about toys",
                    CategoryID = 3
                },
                new
                {
                    Title = "The Goonies",
                    MovieID = 15,
                    Description = "Movie about Kids on an adventure",
                    CategoryID = 5
                },
                new
                {
                    Title = "E.T. The Extra-Terrestrial",
                    MovieID = 16,
                    Description = "Movie about ET",
                    CategoryID = 5
                },
                new
                {
                    Title = "High Noon",
                    MovieID = 17,
                    Description = "Movie about cowboys",
                    CategoryID = 6
                }



                 ///
                 //1
                 //Notting Hill 
                 //When Harry Met Sally
                 //3
                 //Snow White and the Seven Dwarfs(1937)
                 //Fantasia(1940)
                 //Beauty and the Beast(1991)
                 //Toy Story(1995)
                 //5
                 //The Goonies(1985)
                 //Suitable for: Kids ages 10 +; Rated PG
                 //Run time: 114 minutes
                 //This coming - of - age ’80s classic has got it all: hidden treasure, everlasting friendship, 
                 //edge - of - your - seat thrills and a young Josh Brolin.The bad guys(the thieving Fratellis) are a little scary, 
                 //which is why we recommend saving this one for kiddies ten years and up.

                 //E.T. The Extra-Terrestrial (1982)

                 //        6
                 //High Noon(1952)
                 //The Searchers(1956)
                 );
        }
    }
}





