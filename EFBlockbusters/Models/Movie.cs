﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFBlockbusters.Models
{
    public class Movie
    {
        public int MovieID { get; set; }
        public string Title { get; set; }
      
        [MaxLength(400)] 
        public string Description { get; set; }
        public Category Category { get; set; }
    }
}
