﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFBlockbusters.Models
{
    public class Category
    {

        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public ICollection<Movie> Movies { get; set; }
    }
}

