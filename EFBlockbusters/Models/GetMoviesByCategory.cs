﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFBlockbusters.Models
{
    public partial class GetMoviesByCategory
    {
        public int MovieID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
