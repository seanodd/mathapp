﻿using EFBlockbusters.Models;
using Microsoft.EntityFrameworkCore;

namespace EFBlockbusters;

class Program
{
    static void Main(string[] args)
    {
        DisplayCategories();

        //GetMoviesByCategory("Action");

        Console.Write("What type of movies would you like to see? ");
        string category = Console.ReadLine();

        Console.WriteLine("ID Title Description");
        Console.WriteLine($"{"ID",5} | {"Title",40} | {"Description",50}");
        Console.WriteLine($"{"-----",5} | {"----------------------------------------",40} | {"--------------------------------------------",50}");
        GetMoviesByCategory(category);
        //using (var context = new BlockbusterMovieEntities())
        //{
        //    var movieAndCategoryQuery =
        //    from m in context.Movies
        //    join c in context.Categories
        //    on m.Category equals c
        //    where c.CategoryName == category
        //    orderby c.CategoryName
        //    select new
        //    {
        //        MovieID = m.MovieID,

        //        Title = m.Title,             
        //        Description = m.Description
        //    };
        //    foreach (var m in movieAndCategoryQuery)
        //    {
        //        Console.WriteLine($"{m.MovieID,5} | {m.Title,40} | {m.Description,50}");
        //    }
        //}
    }

    private static void GetMoviesByCategory(string category)
    {
        using (var context = new BlockbusterMovieEntities())
        {
            var MoviesByCategory =
                 context.GetMoviesByCategory.FromSql(
                 $"[GetMoviesByCategory] {category}").ToList();
            foreach (var m in MoviesByCategory)
            {
                Console.WriteLine($"{m.MovieID,5} | {m.Title,40} | {m.Description,50}");
            }
        }
    }

    public static void DisplayCategories()
    {
        using (var context = new BlockbusterMovieEntities())
        {
            var allCategoriesQuery = from s in context.Categories
                                   select s;
            foreach (Category c in allCategoriesQuery)
            {
                Console.WriteLine(
                $"#{c.CategoryID} {c.CategoryName} ");
            }

        }
    }
}