﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace EFBlockbusters.Migrations
{
    /// <inheritdoc />
    public partial class SeedMovies4 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 8,
                column: "Description",
                value: "Movie about NYC police officer saving hostages from building");

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "MovieID", "CategoryID", "Description", "Title" },
                values: new object[,]
                {
                    { 11, 1, "Movie with Hugh Grant", "Notting Hill" },
                    { 12, 1, "Movie about Harry", "When Harry Met Sally" },
                    { 13, 3, "Movie about a Beast", "Beauty and the Beast" },
                    { 14, 3, "Story about toys", "Toy Story" },
                    { 15, 5, "Movie about Kids on an adventure", "The Goonies" },
                    { 16, 5, "Movie about ET", "E.T. The Extra-Terrestrial" },
                    { 17, 6, "Movie about cowboys", "High Noon" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 17);

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 8,
                column: "Description",
                value: "Movie NYC police officer saving hostages from building");
        }
    }
}
