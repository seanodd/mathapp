﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace EFBlockbusters.Migrations
{
    /// <inheritdoc />
    public partial class SeedMovies2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "MovieID", "CategoryID", "Description", "Title" },
                values: new object[,]
                {
                    { 2, 4, "Movie about Luke Skywalker", "Star Wars: Episode IV - A New Hope" },
                    { 3, 4, "Movie about Blade Runners", "Blade Runner" },
                    { 4, 4, "Movie about Neo", "The Matrix" },
                    { 5, 4, "Movie about batman", "The Dark Knight" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 5);
        }
    }
}
