﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFBlockbusters.Migrations
{
    /// <inheritdoc />
    public partial class SeedMovies1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "MovieID", "CategoryID", "Description", "Title" },
                values: new object[] { 1, 4, "When Dr. Dave Bowman (Keir Dullea) and other astronauts are sent on a mysterious mission, their ship's computer system, HAL, begins to display increasingly strange behavior.", "2001: A Space Odyssey" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 1);
        }
    }
}
