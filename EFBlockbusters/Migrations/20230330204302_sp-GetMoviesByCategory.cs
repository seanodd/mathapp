﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFBlockbusters.Migrations
{
    /// <inheritdoc />
    public partial class spGetMoviesByCategory : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //var movieAndCategoryQuery =
            //from m in context.Movies
            //join c in context.Categories
            //on m.Category equals c
            //where c.CategoryName == category
            //orderby c.CategoryName
            //select new
            //{
            //    MovieID = m.MovieID,

            //    Title = m.Title,
            //    Description = m.Description

            var sp =
 @"CREATE PROCEDURE GetMoviesByCategory
    @CategoryName nvarchar(15)
AS


SELECT m.MovieID, m.Title, m.Description 
	
FROM Movies m, Categories c
WHERE m.CategoryID = c.CategoryID

    AND c.CategoryName = @CategoryName


ORDER BY m.MovieID";



            migrationBuilder.Sql(sp);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
