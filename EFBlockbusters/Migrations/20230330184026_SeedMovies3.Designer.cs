﻿// <auto-generated />
using EFBlockbusters.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace EFBlockbusters.Migrations
{
    [DbContext(typeof(BlockbusterMovieEntities))]
    [Migration("20230330184026_SeedMovies3")]
    partial class SeedMovies3
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.4")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("EFBlockbusters.Models.Category", b =>
                {
                    b.Property<int>("CategoryID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("CategoryID"));

                    b.Property<string>("CategoryName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("CategoryID");

                    b.ToTable("Categories");

                    b.HasData(
                        new
                        {
                            CategoryID = 1,
                            CategoryName = "Romantic Comedy"
                        },
                        new
                        {
                            CategoryID = 2,
                            CategoryName = "Action"
                        },
                        new
                        {
                            CategoryID = 3,
                            CategoryName = "Kids"
                        },
                        new
                        {
                            CategoryID = 4,
                            CategoryName = "Science Fiction"
                        },
                        new
                        {
                            CategoryID = 5,
                            CategoryName = "Family"
                        },
                        new
                        {
                            CategoryID = 6,
                            CategoryName = "Western"
                        });
                });

            modelBuilder.Entity("EFBlockbusters.Models.Movie", b =>
                {
                    b.Property<int>("MovieID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MovieID"));

                    b.Property<int>("CategoryID")
                        .HasColumnType("int");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(400)
                        .HasColumnType("nvarchar(400)");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("MovieID");

                    b.HasIndex("CategoryID");

                    b.ToTable("Movies");

                    b.HasData(
                        new
                        {
                            MovieID = 1,
                            CategoryID = 4,
                            Description = "When Dr. Dave Bowman (Keir Dullea) and other astronauts are sent on a mysterious mission, their ship's computer system, HAL, begins to display increasingly strange behavior.",
                            Title = "2001: A Space Odyssey"
                        },
                        new
                        {
                            MovieID = 2,
                            CategoryID = 4,
                            Description = "Movie about Luke Skywalker",
                            Title = "Star Wars: Episode IV - A New Hope"
                        },
                        new
                        {
                            MovieID = 3,
                            CategoryID = 4,
                            Description = "Movie about Blade Runners",
                            Title = "Blade Runner"
                        },
                        new
                        {
                            MovieID = 4,
                            CategoryID = 4,
                            Description = "Movie about Neo",
                            Title = "The Matrix"
                        },
                        new
                        {
                            MovieID = 5,
                            CategoryID = 4,
                            Description = "Movie about batman",
                            Title = "The Dark Knight"
                        },
                        new
                        {
                            MovieID = 6,
                            CategoryID = 2,
                            Description = "Movie about Max.",
                            Title = "Mad Max"
                        },
                        new
                        {
                            MovieID = 7,
                            CategoryID = 2,
                            Description = "Movie about Aliens",
                            Title = "Aliens"
                        },
                        new
                        {
                            MovieID = 8,
                            CategoryID = 2,
                            Description = "Movie NYC police officer saving hostages from building",
                            Title = "Die Hard"
                        },
                        new
                        {
                            MovieID = 9,
                            CategoryID = 2,
                            Description = "Movie about Terminators",
                            Title = "Terminator 2: Judgment Day"
                        },
                        new
                        {
                            MovieID = 10,
                            CategoryID = 2,
                            Description = "Movie about James bond",
                            Title = "Casino Royale"
                        });
                });

            modelBuilder.Entity("EFBlockbusters.Models.Movie", b =>
                {
                    b.HasOne("EFBlockbusters.Models.Category", "Category")
                        .WithMany("Movies")
                        .HasForeignKey("CategoryID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Category");
                });

            modelBuilder.Entity("EFBlockbusters.Models.Category", b =>
                {
                    b.Navigation("Movies");
                });
#pragma warning restore 612, 618
        }
    }
}
