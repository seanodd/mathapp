﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace EFBlockbusters.Migrations
{
    /// <inheritdoc />
    public partial class SeedMovies3 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "MovieID", "CategoryID", "Description", "Title" },
                values: new object[,]
                {
                    { 6, 2, "Movie about Max.", "Mad Max" },
                    { 7, 2, "Movie about Aliens", "Aliens" },
                    { 8, 2, "Movie NYC police officer saving hostages from building", "Die Hard" },
                    { 9, 2, "Movie about Terminators", "Terminator 2: Judgment Day" },
                    { 10, 2, "Movie about James bond", "Casino Royale" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieID",
                keyValue: 10);
        }
    }
}
